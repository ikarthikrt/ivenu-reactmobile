/**
 * @format
 */

import {AppRegistry} from 'react-native';
import Intro from './intro';
import {name as appName} from './app.json';
console.disableYellowBox=true;
AppRegistry.registerComponent(appName, () => Intro);

import React,{Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,Image,Alert,FlatList,} from 'react-native';
import {Right,Top,Icon, Button} from 'native-base'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import StarRating  from 'react-native-star-rating'
import Line from './Line'
import Text_box from './text_box'

export default class Book_your_venue extends Component{
    constructor(props) {
        super(props);
        this.state= {
           ratingCount:3
        }       
    }
    onStarRatingPress(rating) {
        this.setState({
          ratingCount: rating
        });
      }
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.flex_row}>
                    <Text style={styles.text11}>Hello</Text>
                    <Text style={styles.text1}> Daissy Murphy</Text>
                    <Text style={styles.text1}>.</Text>
                </View>
                <View style={styles.view2}>
                    <Text style={styles.text2}>Book Your venue.</Text>
                    <Text style={styles.text2}>|.</Text>
                    <View style={styles.view3}>
                        <Text style={styles.text7}>Change Venue</Text>
                    </View>
                </View>
                <View style={styles.view4}>
                    <Text style={styles.text4}>Soccer Club Academy</Text>
                </View>
                <View style={styles.view5}>
                    <Text style={styles.text5}>Add to Cart</Text>
                </View>
                <View>
                    <Line/>
                    <View style={styles.view6}>
                        <View style={{flex:2}}>
                            <StarRating
                                style={styles.ratingStyle}
                                maxStars={5}
                                starSize={15}
                                emptyStarColor={color.ash6}
                                halfStarColor={color.blue}
                                fullStarColor={color.blue}
                                rating={this.state.ratingCount}
                                selectedStar={(rating) => this.onStarRatingPress(rating)}/>
                        </View>
                        <View style={styles.view7}>
                            <Text style={styles.orange_text}>12 Kms</Text>
                            <Text style={styles.orange_text}>|</Text>
                            <Text style={styles.orange_text}>18 Mins</Text>
                        </View>
                        <View style={styles.view8}>
                            <Button style={styles.blue_btn}> 
                                <Text style={styles.btn_txt}>Show Available Slots</Text>
                            </Button>
                        </View>
                    </View>
                    <Line/>
                </View>
                <View style={{flexDirection:'row',paddingTop:5}}>
                    <View style={{flex:.15}}>

                    </View>
                    <View style={{flex:.7}}>
                        <View style={styles.view9}>
                            <View style={{flex:.4}}>
                                <Text>Name</Text>
                            </View>
                            <View style={{flex:.6}}>
                                <Text_box></Text_box>
                            </View>
                        </View>
                        <View style={styles.view9}>
                            <View style={{flex:.4}}>
                                <Text>When</Text>
                            </View>
                            <View style={{flex:.6}}>
                                <Text_box > </Text_box>
                            </View>
                        </View>
                        <View style={styles.view9}>
                            <View style={{flex:.4}}>
                                <Text>Time Slot</Text>
                            </View>
                            <View style={{flex:.6}}>
                                <Text_box > </Text_box>
                            </View>
                        </View>
                        <View style={styles.view9}>
                            <View style={{flex:.4}}>
                                <Text>Price</Text>
                            </View>
                            <View style={{flex:.6,flexDirection:'row'}}>
                                <View style={{flex:1}}>
                                    <Text_box > </Text_box>
                                </View>
                                <View style={styles.view10}>
                                    <Text style={styles.text7}>Use PROMO Code</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.view9}>
                            <View style={{flex:.4}}>
                                <Text>Address</Text>
                            </View>
                            <View style={{flex:.6}}>
                                <Text_box > </Text_box>
                            </View>
                        </View>
                        <View style={styles.view9}>
                            <View style={{flex:.4}}>
                                <Text>Mobile</Text>
                            </View>
                            <View style={{flex:.6}}>
                                <Text_box > </Text_box>
                            </View>
                        </View>
                        <View style={styles.view9}>
                            <View style={{flex:.4}}>
                                <Text>More Info</Text>
                            </View>
                            <View style={{flex:.6}}>
                                <Text_box> </Text_box>
                            </View>
                        </View>
                    </View>  
                    <View style={{flex:.15}}>

                    </View>  

                </View>

                <View style={styles.view11}>
                    <View style={styles.view12}>
                        <Button style={styles.cancel_btn}>
                            <Text style={styles.cancel_txt}> CANCEL </Text>
                        </Button>
                    </View>
                    <View style={styles.view13}>
                        <Button style={styles.book_nw_btn}>
                            <Text style={styles.cancel_txt}> Book NOW </Text>
                        </Button>
                    </View>
                </View>

            </View>
        )
    }
} 
const styles=StyleSheet.create({
    container:{
        backgroundColor:color.white,
        flex:1,
        paddingLeft:8,
        paddingRight:8
    },
    _flex:{
        flex:1
    },
    flex_row:{
        flexDirection:'row'
    },
    text11:{
        fontSize:hp('1.5%')
    },
    text1:{
        fontSize:hp('1.5%'),
        paddingLeft:2,
        fontWeight:'bold'
    },
    text2:{
        color:color.orange,
        fontSize:hp('2.5%')
    },
    text3:{
        paddingLeft:2,
        color:color.blue,
        
    },
    view2:{ 
        flexDirection:'row',
        paddingTop:5
    },
    view3:{
        paddingBottom:2,
        flexDirection:'column',
        justifyContent:'flex-end'
    },
    view4:{
        paddingTop:8
    }, 
    text4:{
        fontSize:hp('3.5%')
    },
    view5:{
        paddingBottom:5
    },
    text5:{
        fontSize:hp('2%'),
        color:color.ash6
    },
    view6:{
        flexDirection:'row',
        paddingTop:8,
        paddingBottom:8
    },
    ratingStyle:{
        justifyContent:'flex-start'

    },
    orange_text:{
        color:color.orange,
        fontSize:hp('1.5%')
    },
    view7:{
        flex:3,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        alignContent:'center',
        alignSelf:'center'
    },
    view8:{
        flex:4.5,
        flexDirection:'row',
        justifyContent:'flex-end',
        alignItems:'flex-end',
        alignContent:'flex-end',
        alignSelf:'flex-end'
    },
    blue_btn:{
        backgroundColor:color.blue,
        height:hp('3%'),
        paddingLeft:8,
        paddingRight:8,
        paddingTop:5,
        paddingBottom:5

    },
    btn_txt:{
        color:color.white,
        fontSize:hp('1.5%'),
        paddingLeft:8,
        paddingRight:8
    },
    view9:{
        flexDirection:'row',
        paddingTop:5,
        paddingBottom:5
    },
    text7:{
        paddingLeft:2,
        color:color.blue,
        fontSize:hp('1.5%')   
    },
    view10:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        alignContent:'center',
        alignSelf:'center'
    },
    view11:{
        flexDirection:'row',
        paddingTop:15,
        paddingBottom:15
    },
    view12:{
        flex:1,
    },
    view13:{
        flex:1,
        justifyContent:'flex-end',
        flexDirection:'row'
    },
    cancel_txt:{
        fontSize:hp('2%'),
        color:color.white
    },
    cancel_btn:{
        paddingLeft:10,
        paddingRight:10,
        backgroundColor:color.blue,
        height:hp('5%')
    },
    book_nw_btn:{
        backgroundColor:color.orange,
        paddingLeft:8,
        paddingRight:8,
        height:hp('5%')
    }

})
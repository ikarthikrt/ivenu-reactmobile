import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,ScrollView,FlatList,} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import Down_arrow from '../images/arrow_down.png'
import ADD_img from '../images/add_icon.png'
import Greyright from '../images/greyright.png'
import ModalDropdown  from 'react-native-modal-dropdown'
import links from '../Helpers/config';

const fav_list=[
    {fav:'Birthday Party',id:0},
    {fav:'Soccer Ground',id:1},
    {fav:'Family Gathering Venues',id:2},
    {fav:'Meeting Hall',id:3},
    {fav:'Wedding Venue',id:4},
   ];

export default class YourFavorites extends Component{
    constructor(props) {
      super(props);
    
      this.state = {
        listFav:[]
      };
    }
    componentWillMount(){
    this.LookforList();
  
  }
    LookforList=()=>{
          fetch(links.APIURL+'youMayLookFor', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:"",
    }).then((response)=>response.json())
    .then((responseJson)=>{
      // console.log("you may look for response",responseJson.data);
      this.setState({listFav:responseJson})
      // alert(JSON.stringify(responseJson));


    })
    }
    dropdownShow=(data)=>{
        // alert(JSON.stringify(data));
        return false;
    }
    render(){
        return(
            <View style={styles.container}>
                <View style={[styles.flex_row,styles.text_center]}>
                    <Text style={styles.text_near}> Your </Text>
                        <ModalDropdown   
                                    style={{marginRight:3}}    
                                    textStyle={[styles.text_fav]}
                                    dropdownStyle={styles.dropdown_options}
                                    defaultValue={'Favorites'}
                                    showsVerticalScrollIndicator={false}    
                                       onDropdownWillShow={this.dropdownShow}                          
                                options={['Near You', 'Already Booked','with OFFER *','Low Cost']} />
                        {/*<Image source={Down_arrow} style={styles.down_aarow}></Image>*/}
                </View>
                <View style={styles.flex_row}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                    {this.state.listFav&&this.state.listFav.map((item)=>{
                        return(
                            <TouchableOpacity style={styles.square} onPress={()=>this.props.sendFavouries&&this.props.sendFavouries(item)}>
                            <Text style={styles.text_style}>
                                    {item.venue_cat_name}
                            </Text>
                        </TouchableOpacity>
                            )
                    })}
                        

                        
                       
                        
                       </ScrollView> 

                    <View style={styles.right_arr_container}>
                        <Image source={Greyright} style={styles.right_arr_style}></Image>
                    </View> 
                </View>   
                {this.state.listFav.length==0&&
                               <View ><Text style={{textAlign:'center'}}>No Records</Text></View>
                        }
            </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        paddingTop:hp('1%'),
        paddingBottom:hp('1%'),
        backgroundColor:color.white
    },
    square:{
        margin:5,
        height:hp('8%'),
        width:hp('10%'),
        borderRadius:5,
        backgroundColor:color.orange,
        justifyContent:'center',
        padding:2
            
    },
    text_style:{
        color:color.white,
        textAlign:"center",
        fontSize:hp('1.6%'),
    },
    flex_row:{
        flexDirection:'row'
    },
    text_center:{
        alignItems:'center',
    },
    text_near:{
        fontSize:hp('1.9%'),
       
    },
    text_fav:{
        fontSize:hp('1.9%'),
        color:color.blue,
        fontWeight:'bold'    
    },
    down_aarow:{
        height:hp('1%'),
        width:hp('1%'),
        justifyContent:'flex-end',
        alignItems:'flex-end',
        alignSelf:'flex-end',
        alignContent:'flex-end',
        margin:3
    },
    right_arr_style:{
        height:hp('2%'),
        width:hp('2%'),
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center',
        margin:3
    },
    right_arr_container:{
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center',
        backgroundColor:color.white
    },
    fav_item_container:{
        paddingTop:hp('1%'),
        paddingBottom:hp('1%'),
        paddingLeft:('1%')
    },
    add_symbol_container:{
        backgroundColor:color.blue,
        justifyContent:'flex-end',
        justifyContent:'center',
        alignSelf:'flex-end',
        marginRight:wp('5%'),
        height:hp('2.5%'),
        width:hp('2.5%'),
       
    },
    
})
'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  TouchableOpacity
} from 'react-native';
import Toast from 'react-native-simple-toast';
import { GoogleSignin, GoogleSigninButton,statusCodes } from 'react-native-google-signin';

class GoogleLogin extends Component {
	signIn = async () => {
  try {
    await GoogleSignin.hasPlayServices();
    const userInfo = await GoogleSignin.signIn();
    // console.log(userInfo);
    var profile=userInfo.user;
     var  id=profile.id; // Don't send this directly to your server!
        var fullname = profile.name;
        var firstname = profile.givenName;
        var lastname =  profile.familyName;
        var image = profile.photo;
        var email = profile.email;
        var obj={firstName:firstname,lastName:lastname,picture:image,email:email,oauthProvider:'google',outhId:id};
          this.props.sendSocialDetails&&this.props.sendSocialDetails(obj);
    // this.setState({ userInfo });
  } catch (error) {
  	console.log(error);
    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
    	console.log("cancelled")
                  Toast.show('User cancelled', Toast.LONG)
      // user cancelled the login flow
    } else if (error.code === statusCodes.IN_PROGRESS) {
    	console.log("progress");
                  Toast.show('In Progress', Toast.LONG)

      // operation (f.e. sign in) is in progress already
    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
    	// console.log("not available")
    	Toast.show('Play Service Not Available', Toast.LONG)
      // play services not available or outdated
    } else {
      // some other error happened
    }
  }
};
  render() {
    return (
      <TouchableOpacity onPress={this.signIn}>
      {this.props.children}
      </TouchableOpacity>
    );
  }
 componentDidMount(){
	GoogleSignin.configure({// what API you want to access on behalf of the user, default is email and profile
  webClientId: '991993261926-40eh1aficetf270dknlf6ut67edj5b74.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
  offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  hostedDomain: '', // specifies a hosted domain restriction
  forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login
  accountName: '', // [Android] specifies an account name on the device that should be used
  iosClientId: '', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
});
}
}

const styles = StyleSheet.create({

});


export default GoogleLogin;

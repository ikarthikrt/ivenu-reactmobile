import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { Button, Right } from "native-base";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen"; 
import dateFormat from "dateformat";
import moment from "moment";
import color from '../Helpers/color';
import PropTypes from 'prop-types';


export default class Schedule1 extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     selected: [],
                     list: props.list,
                     edit:false
                   };
                   this._onPressButton = this._onPressButton.bind(this);
                 }



                 _submit=()=>{ 

                   let checkedListSlots = this.state.list.filter((item,i)=>item.checked==true);
                   if (checkedListSlots.length) 
                   { 
                       this.props.onSubmit(checkedListSlots);
                   }
                  
                   else return;
    
                 }

                

                 _onPressButton = (value, checked) => {
                     console.log('onpress',value.id)

                     var findIndex=this.state.list.findIndex(obj=>
                        obj.id==value.id)
                        console.log('find index',findIndex)
                        console.log('check',checked)
                     
                    var list = this.state.list;
                    if (checked == null || findIndex >=0)
                      { console.log('if')
                        list[findIndex].checked = checked ? !checked : true;
                      }
                    else {
                      console.log('else')
                      list[findIndex].checked = true;
                    }

                    this.setState({ list });
                    console.log('list',list)

                    this.props.onClick(
                      this.state.list.filter(value => value.checked == true)
                    );

                    if(value.edit !=null &&value.edit==true)
                     this.props.unVisibleEditSelected(value)

                                                         };

                 componentDidUpdate = (prevProps, prevState) => {
                
                 }; 

                 componentWillMount=()=>{
                   
                    
 
                 }

                

                 renderTimeFormat = date => {
                   return moment(date, "HH:mm").format("hh:mm A");
                 };

                 componentWillReceiveProps(nextProps) {
                    //update modal visible 
                   if (nextProps.edit !== this.props.edit) {
                     
                     this.setState({ edit: nextProps.edit });
                    
                   }

                  }

                 renderSchedule = isOnClick => { 

                     const {list,edit} = this.state; 
                   
                     console.log('list',list)


                   if(this.props.edit){ 
                   
                     var map = this.props.list.filter(
                       (ok, j) =>
                         ok.checked ||
                         ok.edit == true ||
                         ok.visible != false 
                        
                     );
                   }else if(!this.props.static){
                     var map = this.props.list.filter(
                       (ok, j) => ok.visible == null || ok.visible != false
                     );
                   }
                   else
                   var map = this.props.list

                   return (
                     this.props.list &&
                     map.map((value, i) => { 
                       //  alert();
                       // console.log(value.checked);
                       var findindex = this.state.selected.findIndex(
                         obj => obj == value
                       );
                       let selected; 

                       if (
                        value.checked == true &&
                         this.props.static != true 
                       ) {
                         //  console.log("iruku");
                         selectedBox = {
                           backgroundColor: "#eb5b00",
                           color: "white"
                         };
                       } else selectedBox = { backgroundColor: "white" };

                       var notAvail =
                         value.bookingslottype == 2 ||
                         value.bookingslottype == 1
                           ? {
                               backgroundColor: "rgb(210,213,227)",
                               borderColor: "rgb(50,65,146)",
                               borderWidth: 1
                             }
                           : {}; 




                       return (
                         <View style={[styles.box]} key={i}>
                           <TouchableOpacity
                             onPress={() =>
                              isOnClick
                                 ? this._onPressButton(value, value.checked)
                                 :""
                             }
                             style={[styles.button, selectedBox, notAvail]}
                           >
                             <Text
                               style={[
                                 {
                                   textAlign: "center"
                                 },
                                 selectedBox,
                                 value.bookingslottype == 2 ||
                                 value.bookingslottype == 1
                                   ? {
                                       backgroundColor: "transparent",
                                       color: "rgb(50,65,146)"
                                     }
                                   : {}
                               ]}
                             >
                              {value.label}
                             </Text>
                           </TouchableOpacity>
                         </View>
                       );
                     })
                   );
                 };

                 render() {
                   const { boxColor, isOnClick } = this.props;
                   console.log('edit',this.props.edit)
                   return (
                     <React.Fragment>
                       <View
                         style={[
                           styles.container,
                           { backgroundColor: boxColor }
                         ]}
                       >
                         
                         {this.props.list.length <= 0 && (
                           <View
                             style={{
                               flex: 1,
                               paddingHorizontal: 30,
                               paddingVertical: 30,
                               justifyContent: "center",
                               alignItems: "center"
                             }}
                           >
                             <Text style={{ fontSize: 22,textAlign:'center' }}>
                               
                             No Records Found {"  "}
                             </Text>
                           
                           </View>
                         )}
                         {this.props.list.length>0&&this.renderSchedule(isOnClick)}
                       </View>

                       <View
                         style={{
                           flexDirection: "row",
                           justifyContent: "flex-end",
                           alignItems: "flex-end",
                           paddingRight: 30
                         }}
                       ></View>
                     </React.Fragment>
                   );
                 }
               } 


               Schedule1.propTypes = {
                 list: PropTypes.array.isRequired,
                 boxColor: PropTypes.any,
                 edit:PropTypes.bool,
                 static:PropTypes.bool,
                 isOnClick:PropTypes.oneOfType([
                   PropTypes.func,PropTypes.bool,
                 ]).isRequired,
               };  

               Schedule1.defaultProps = {
                list:[],
                boxColor:'#f1f1f1',
                static:false,
                edit:false
               };

               

const styles = StyleSheet.create({
  button: {
    backgroundColor:'#fff',
    color: "#000",
    minWidth: wp("40%"),
    marginTop:4,
    padding:12
  },
  box: {
    paddingVertical: hp("1.4%")
  },
  container: {
    flex: 1,
    flexDirection: "row",
    padding: 20,
    justifyContent: "space-between",
    flexWrap: "wrap",
    
  },
   actionbtn: {
                   borderRadius: 5,
                   width: hp("17%"),
                   justifyContent: "center",
                   backgroundColor: color.blue
                 },
                 actionbtntxt: {
                   textAlign: "center",
                   color: color.white,
                   fontSize: hp("2.3%")
                 }
});

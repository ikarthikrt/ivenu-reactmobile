import React, { Component } from 'react';
import {Image, Platform,TouchableHighlight, StyleSheet, Text, View, BackHandler, Alert, TouchableOpacity,ScrollView } from 'react-native';
import { Router, Scene,Actions } from 'react-native-router-flux'
import {H3,Container, Content, Header, Left, Body, Right, Button, Icon, Title,List,ListItem, } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import color from '../Helpers/color';
import placeholder_img from '../images/placeholder_img.png';
import VenueCardComp from '../components/VenueCardComp';
export default class SessionList extends Component{
	venuePage=()=>{
		if(this.props.action==false){

			// alert("actions not configured");
		}else{
	//	Actions.venuepage();
		
		}
	}
	sideClick=()=>{
		this.scrollimage.scrollToEnd();
	}
	render(){
		// alert(JSON.stringify(this.props.imageList));
		return(
			<View style={styles.flexrow}>
			    <View style={styles.col3}>
                    <Text style={[styles.alignLeft,{color:color.top_red,fontSize:hp('2.3%'),paddingRight:12}]}>
                        {this.props.title}
                    </Text>
                    <TouchableOpacity onPress={()=>this.venuePage()}>
                       
                        <View style={{flexDirection:'row',marginTop:hp('1%'),}}>
                            <Text style={{fontSize:hp('1.8%'),color:color.blueactive,marginRight:hp('1.5%')}}>{this.props.subtitle}</Text>
                            <View style={{flexDirection:'column',justifyContent:'center',marginLeft:2,}}>
                            {this.props.icon && 
                                <Icon style={{fontSize:hp('2%'),color:color.grey}} type="FontAwesome" name="angle-right" /> 
                            } 
                            </View>
                        </View>

                    </TouchableOpacity>
			    </View>
			
		        <View style={styles.col7}>
			        <ScrollView style={{flex:1}} horizontal={true} showsHorizontalScrollIndicator={false} ref={(c) => {this.scrollimage = c}}>
			            {this.props.imageList&&this.props.imageList.map((item,key)=>{
				        return(
				       <TouchableOpacity onPress={()=>this.props.sendVenueDetails(item)} style={[styles.listImage,{height:'auto'}]}>
			              <VenueCardComp item={item}/>
                      </TouchableOpacity>
			            )
			            })
			            }
                    </ScrollView>
                    <TouchableOpacity style={styles.rightArrw}  onPress={this.sideClick}>
	                    <Icon style={{fontSize:hp('3%'),color:color.ash3}} type="FontAwesome" name="angle-right" />	
	                </TouchableOpacity> 
			  </View>
			</View>
		)
	}
}
const styles=StyleSheet.create({
	listImage:{
		width:hp('18%'),
		height:hp('11%'),
		borderRadius:5,
		marginRight:7
	},
	rightArrw:{
		flexDirection:'row',
		alignItems:'center',
		height:'100%',
		paddingLeft:7
	},
    col3:{
    	flex:0.3
    	,justifyContent:'center',
    },
    col7:{
    	flex:0.7,
    	flexDirection:'row'
    },
    flexrow:{
        flex:1,
        flexDirection:'row',
        // margin:12,
        padding:12,
        borderBottomWidth:0.5
    },
})
import React,{Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,Image,Alert,FlatList,} from 'react-native';
import {Right,Top,Icon, Button,ListItem,} from 'native-base'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import fb_png from '../images/fb.png';
import insta_png from '../images/insta.png';
import google_png from '../images/google.png'

export default class Search_Sccess extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={{flex:.1}}></View>
                <View style={styles.view1}>
                    <View style={styles.view2}>
                        <Text style={styles.txt1}>Hello</Text>
                        <Text style={styles.txt2}>Daisy Murphy</Text>
                    </View>
                    <View style={styles.view3}>
                        <Text style={styles.txt3}>You have booked the Venue</Text>
                        <Text style={styles.txt4}>Successfully!</Text>
                    </View>
                    <View style={styles.view4}>
                        <Text style={styles.txt5}>Booking Details</Text>
                    </View>
                    <View style={styles.view5}>
                        <Text style={styles.txt6}>Soccer Club Academy</Text>
                        <Text style={styles.txt5}>06-08 AM, 18 May 2019</Text>
                    </View>
                </View>    
                <View style={{flex:.1}}>
                    <View style={styles.view6}>
                        <Text style={styles.txt7}>Share with</Text>
                        <TouchableOpacity><Image style={styles.socialicon} source={google_png}/></TouchableOpacity>
	                    <TouchableOpacity><Image style={styles.socialicon} source={fb_png}/></TouchableOpacity>
	                    <TouchableOpacity><Image style={styles.socialicon} source={insta_png}/></TouchableOpacity>
                    </View>
                </View>
                
                <View style={{flex:.1,flexDirection:'row'}}>
                    <View style={styles.view12}>
                        <Button style={styles.cancel_btn}>
                            <Text style={styles.cancel_txt}> BOOK MORE </Text>
                        </Button>
                    </View>
                    <View style={styles.view13}>
                        <Button style={styles.cancel_btn}>
                            <Text style={styles.cancel_txt}> ADD VENUE </Text>
                        </Button>
                    </View>
                </View>    
            </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:color.white, 
        paddingLeft:hp('2%'),
        paddingRight:hp('2%')
    },
    view1:{
        flex:.5,
        flexDirection:'column',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center'
    },
    view2:{
        flexDirection:'row'
    },
    view3:{
        marginTop:hp('2%'),
        marginBottom:hp('2%')
    },
    view4:{
        marginBottom:hp('2%')
    },
    view5:{
        marginTop:hp('2%'),  
    },
    view6:{
        flexDirection:'row',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center'
    },



    txt1:{
        marginRight:3,
        fontSize:hp('2%')
    },
    txt2:{
        fontWeight:'bold',
        fontSize:hp('2%')
    },
    txt3:{
        color:color.orange,
        fontSize:hp('2.5%')
    },
    txt4:{
        color:color.orange,
        fontSize:hp('3%'),
        flexDirection:'row',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center'
    },
    txt5:{
        fontSize:hp('2.5%'),
        flexDirection:'row',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center'
    },
    txt6:{
        fontSize:hp('3%'),
        flexDirection:'row',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center'
    },
    txt7:{
        color:color.black,
        fontSize:hp('2%'),
        flexDirection:'row',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center',
        marginRight:8,
        
    },
    socialicon:{
		width:hp('4%'),
		height:hp('4%'),
        margin:2
    },
    view12:{
        flex:1,
        justifyContent:'flex-end',
    },
    view13:{
        flex:1,
        justifyContent:'flex-end',
        alignSelf:'flex-end',alignContent:'flex-end',alignItems:'flex-end',
        flexDirection:'row'
    },
    cancel_txt:{
        fontSize:hp('2%'),
        color:color.white
    },
    cancel_btn:{
        paddingLeft:10,
        paddingRight:10,
        backgroundColor:color.orange,
        height:hp('5%')
    },
})
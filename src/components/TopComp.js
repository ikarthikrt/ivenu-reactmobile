import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,TouchableHighlight,FlatList,ActivityIndicator } from 'react-native';
import {Actions,Router,Scene} from 'react-native-router-flux'
import {Right,Top,Icon,Container} from 'native-base'
import color from '../Helpers/color'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ModalDropdown  from 'react-native-modal-dropdown'
import Toast from 'react-native-simple-toast';
import VideoComp from '../components/VideoComp'
import Home_venuelist from '../components/home_venuelist'
import White_square from '../components/white_circle_sqaure'
import Right_arrow from '../images/rightarrow.png'
import Home_image from '../images/home_image.jpg'
import Video_png from '../images/VideoPNG/PlayPNG.png'
import Down_arrow from '../images/arrow_down.png'
import ADD_img from '../images/add_icon.png'
import Greyright from '../images/greyright.png'
import FooterComp from '../components/Footer'
import Modeldropdowncomp from '../components/ModeldropdownComp'
import Dropdown_conatiner from '../components/drpdown_container'
import LoadingOverlay from '../components/LoadingOverlay';
import Geolocation from 'react-native-geolocation-service';
import links from '../Helpers/config';


var do_options=['Celebrate', 'Play','Party','Gather','Meet','Conference']
var what_options=['Basketball','Cricket','Tennis','Base Ball','Table Tennis','Badminton']
var where_options=['Old Trafford','San Siro','OlympiaStadion']
export default class TopComp extends Component{
    constructor(props) {
      super(props);
      this.state = {loading:false,doDropdown:{id:'venue_act_id',name:'venue_act_name',dropdown:[]},whatDropdown:{id:'venue_purpose_id',name:'venue_purpose_name',dropdown:[]},whereDropdown:{id:'venue_cat_id',name:'venue_cat_name',dropdown:[]},dodata:null,whatdata:null,wheredata:null,TOS:0,chosenlocation:{latitude:0,longitude:0}};
    }

    _dropdown_5_show() {
        this._dropdown_5 && this._dropdown_5.show();
    
        //   return false;
      }
      _dropdown_5_willShow() {
        this._dropdown_5 && this._dropdown_5.show();
    //     return false;
      }
       sendDropdownData=(data,key)=>{
          this.setState({[key]:data})
          if(key=='dodata'){
  this.setState({whatdata:null,TOS:1})
  this.clearwhat();
  this.clearwhere();
this.loadwhat(data.venue_act_id); 
}else if(key=='whatdata'){
    this.clearwhere();
this.setState({wheredata:null,TOS:2})
this.loadwhere(data.venue_purpose_id,this.state.dodata.venue_act_id);
}else if(key=='wheredata'){
    this.setState({TOS:3})
}
      }
    clearwhat=()=>{
            var whatDropdown=this.state.whatDropdown;
            whatDropdown.dropdown=[];
            this.setState({whatDropdown,whatdata:null});
          }
     clearwhere=()=>{
            var whereDropdown=this.state.whereDropdown;
            whereDropdown.dropdown=[];
            this.setState({whereDropdown,wheredata:null});
          }

      _changedrop(){
          this.setState(backgroundColor=color.orange)
      }
loaddo(){
    fetch(links.APIURL+'do', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
        var doDropdown=this.state.doDropdown;
    doDropdown.dropdown=responseJson.data;
    this.setState({doDropdown})
    console.log(this.state.doDropdown);
    // alert(JSON.stringify(responseJson.data));
    // alert(JSON.stringify(this.state.doDropdown));

    })  
  }

  loadwhat(data){
    fetch(links.APIURL+'whatpurpose', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({'venue_act_id':data}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
    var whatDropdown=this.state.whatDropdown;
    whatDropdown.dropdown=responseJson.data;
    this.setState({whatDropdown})
     // alert(JSON.stringify(responseJson.data));
    // console.log("what",this.state.whatDropdown);
    })  
  }

  loadwhere(data,data1){
    // console.log("where_id",data);
    fetch(links.APIURL+'wherecategory', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({'venue_act_id':data1,'venue_purpose_id':data}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
  var whereDropdown=this.state.whereDropdown;
    whereDropdown.dropdown=responseJson.data;
    this.setState({whereDropdown})
     // alert(JSON.stringify(responseJson.data));
    // console.log("where",responseJson);

    })  
  }
arrowClick=()=>{
  this.setState({loading:true})
  fetch(links.APIURL+'dropdownSearchpurpose', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({'venue_act_id':this.state.dodata?this.state.dodata.venue_act_id:null,'venue_purpose_id':this.state.whatdata?this.state.whatdata.venue_purpose_id:null,'venue_cat_id':this.state.wheredata?this.state.wheredata.venue_cat_id:null,'TOS':this.state.TOS,lat:this.state.chosenlocation.latitude, long:this.state.chosenlocation.longitude}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
  this.setState({loading:false})
        if(responseJson.status&&responseJson.status==1){
            Toast.show('Options are empty to search', Toast.LONG)
        }else{
             if(responseJson.length==0){
               this.props.nxtpge([],'',this.state);
                  Toast.show('No Records Found', Toast.LONG)
             }else{
                this.props.nxtpge(responseJson,'',this.state);
             }
        }
     // Toast.show('This is a long toast.', Toast.LONG);
         // Actions.Search_Home(JSON.stringify(responseJson));
        // alert(JSON.stringify(responseJson));


      // this.setState({searchdata:responseJson});
      // console.log(responseJson);
      // this.props.sendsearchdata(responseJson.data);

    }).catch((resp)=>{
      this.setState({loading:false})
      Toast.show('Network Failed', Toast.LONG)
    })
    //this.props.receivesearch(data);
    //this.setState({visible:true})
  }
      componentWillMount(){
        
        if(this.props.topcompobj){
            // var state=this.state;
            // state=this.props.topcompobj;
            // this.setState({state});
            this.state=props.topcompobj;
        }else{

            this.loaddo();
        }
        Geolocation.getCurrentPosition(
       (position) => {
         
         // alert(JSON.stringify(position));
         this.setState({chosenlocation:{latitude:position.coords.latitude,longitude:position.coords.longitude}});
         //this.getAddressByLatLng(position.coords);
         // this.getAddressByLatLng(position.coords);
       },
       
       (error) => {
                // See error code charts below.
                console.log(error.code, error.message);
            },
       {timeout: 10000,maximumAge:0},
     );
        // this.loadwhat();
        // this.loadwhere();
      } 
      componentWillReceiveProps(props){
    
          if(props.topcompobj){
           
                this.state=props.topcompobj
          }
      }     
      
      
        
    render(){

        // alert(JSON.stringify(this.state))
        return(
                <View style={styles.top_container}>
               
                    <View style={{flex:1,justifyContent:'flex-start',flexWrap:'wrap',flexDirection:'row',alignItems:'center'}}>
                        <Text style={styles.text_style}>
                                I want to
                        </Text>
                    
                       
                       <View style={{paddingLeft:5,paddingRight:5,marginBottom:5}}>   
                            <Dropdown_conatiner
                                drop_items={'Do'}
                                values={this.state.doDropdown.dropdown}
                                keyvalue={"venue_act_name"}
                                action={true}
                                value={this.state.dodata?this.state.dodata.venue_act_name:'Do'}
                                sendDropdownData={(data)=>this.sendDropdownData(data,'dodata')}
                                _width={50}
                            />
                        </View>
                        <View style={{paddingLeft:5,paddingRight:5,marginBottom:5}}>
                            <Dropdown_conatiner
                                drop_items={'What'}
                                 action={true}
                                values={this.state.whatDropdown.dropdown}
                                keyvalue={"venue_purpose_name"}
                                value={this.state.whatdata?this.state.whatdata.venue_purpose_name:'What'}
                                sendDropdownData={(data)=>this.sendDropdownData(data,'whatdata')}
                                _width={50}
                            />
                        </View>     
                        
                        <View style={{alignItems:'center',justifyContent:'center',marginLeft:3,marginRight:3}}>
                            <Text style={styles.text_style}>
                                in
                            </Text>
                        </View>
                       
                        <View style={{paddingLeft:5,paddingRight:5,marginBottom:5}}>
                            <Dropdown_conatiner
                                drop_items={'Where'}
                                values={this.state.whereDropdown.dropdown}
                                keyvalue={"venue_cat_name"}
                                value={this.state.wheredata?this.state.wheredata.venue_cat_name:'Where'}
                                sendDropdownData={(data)=>this.sendDropdownData(data,'wheredata')}
                                _width={50}
                                action={true}

                            />
                        </View>
                        <View style={{justifyContent:'center',}}>
                            <View  style={{marginLeft:3,backgroundColor:color.orange,borderRadius:hp('1.5%'),width:hp('5%')}}>
                            {this.state.loading==true&&
  <ActivityIndicator size="small" color={color.white} style={{paddingTop:3,paddingBottom:3}} />
}
{!this.state.loading&&
                                <TouchableOpacity style={{alignSelf:"center",paddingTop:5,paddingBottom:5}}
                                onPress={()=>this.arrowClick()}> 
                                    <Image style={{width:hp('2%'),height:hp('2%'),alignSelf:"center"}} 
                                         source={Right_arrow}> 
                                    </Image>

                                </TouchableOpacity>
                              }
                            </View>
                        </View>


                </View>
                </View>

                    
        )}
    }
    const styles=StyleSheet.create({
        conatiner:{
            flex:1, 
            backgroundColor:color.white   
        },
        top_container:{
        //    height:hp('10%'),
            paddingTop:hp('1%'),
            paddingBottom:hp('1%'),
            flexDirection:'row',
            backgroundColor:color.top_red,
            padding:hp('2%'),
            paddingLeft:hp('4%')
    
        },
        text_style:{
            color:color.white,
            fontSize:wp('3%')
        },
        drop_down_container:{
        //    flex:1,
            justifyContent:'center',
            marginBottom:hp('2.8%'),
            marginTop:hp('2.8%'),
            marginRight:3,
            minWidth:hp('11%')
        },
        drop_down:{
            backgroundColor:color.white,
            borderRadius:hp('2%'),
            width:hp('10%'),
            paddingLeft:10,
           
                  
        },
        homeBgImage:{
            width:wp('100%'),
            height:hp('25%')
        },
        play_button_container:{
            position:'absolute',
            flexDirection:'row',
            right:0,
            top:0,
            paddingTop:hp('1.5%'),
            paddingRight:hp('1.5%')
        },
        png_style:{
            justifyContent:'center',
            alignItems:'center',
            alignSelf:'center',
            alignContent:'center',
            height:hp('3%'),
            width:hp('3%'),
        },
        png_back:{
            backgroundColor:color.white,
            height:hp('4%'),
            width:hp('4%'),
            borderRadius:hp('4%')/2,
            justifyContent:'center',
            alignItems:'center',
            alignSelf:'center',
            alignContent:'center',
            elevation:1
        },
        flex_row:{
            flexDirection:'row'
        },
        flex_sep:{
            flex:1
        },
        down_aarow:{
            height:hp('1%'),
            width:hp('1%'),
            justifyContent:'flex-end',
            alignItems:'flex-end',
            alignSelf:'flex-end',
            alignContent:'flex-end',
            margin:3
        },
        fav_item_container:{
            paddingTop:hp('1%'),
            paddingBottom:hp('1%'),
            paddingLeft:('1%')
        },
        text_near:{
            fontSize:hp('1.9%'),
           
        },
        text_fav:{
            fontSize:hp('1.9%'),
            color:color.blue,
            fontWeight:'bold'    
        },
        text_center:{
            alignItems:'center',
        },
        line:{
            backgroundColor:color.ash,
            width:'100%',
            height:1
        },
        add_symbol_container:{
            backgroundColor:color.blue,
            justifyContent:'flex-end',
            justifyContent:'center',
            alignSelf:'flex-end',
            marginRight:wp('5%'),
            height:hp('2.5%'),
            width:hp('2.5%'),
           
        },
        add_symbol:{
            height:hp('1.3%'),
            width:hp('1.3%'),
            justifyContent:'center',
            alignItems:'center',
            alignSelf:'center',
            alignContent:'center',
        },
        right_arr_style:{
            height:hp('2%'),
            width:hp('2%'),
            justifyContent:'center',
            alignItems:'center',
            alignSelf:'center',
            alignContent:'center',
            margin:3
        },
        right_arr_container:{
            justifyContent:'center',
            alignItems:'center',
            alignSelf:'center',
            alignContent:'center',
            backgroundColor:color.white
        },
        mar_adjuse:{
            marginTop:3,
            marginBottom:3
        },
        dropdown_container: {
            width:'100%',
            height:hp('4%'),
            justifyContent:'center',
            alignSelf:'center',
           
      },
      dropdown_text:{
     //   fontSize: hp('2%'),
        justifyContent:'center',
      
      },
      dropdown_options:{
        marginTop:10,
        width:wp('30%'),
      },
      drop_down_top:{
        height:hp('1%'),
        width:hp('1%'),
        justifyContent:'center',alignSelf:'center'
      }
    })
    


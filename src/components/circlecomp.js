'use strict';

import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Tabs, Tab, TabHeading,Content } from 'native-base';
import {StyleSheet, View, Image, StatusBar, Picker, Text, TouchableOpacity, ScrollView } from 'react-native';
import color from '../Helpers/color';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

class CircleText extends Component {
  circleStyles=()=>{
return {
  width:hp(this.props.width),
  height:hp(this.props.height),
  borderRadius:hp(this.props.height)/2
}
  } 
   circleCountStyles=()=>{
return {
  width:hp('4%'),
  height:hp('4%'),
  borderRadius:hp('4%')/2,
  alignItems:"center",
  justifyContent:'center'
}
  }
  render() {
    var circlestyles=this.circleStyles();
    return (
      <View style={[styles.circleMain]}>
        <TouchableOpacity
          
          onPress={() => !this.props.disabled&&this.props.sendText(this.props.data)}
          style={[
            styles.circle,
            circlestyles,
            this.props.active ? styles.activecolor : "",
            { borderColor: this.props.innerText ? color.top_red : "orange" },
            this.props.disabled == true ? styles.disablecircle : null
          ]}
        >
          {!this.props.innerText && !this.props.svg && (
            <Image
              tintColor={
                this.props.disabled == true || this.props.active
                  ? "white"
                  : color.blueactive
              }
              style={styles.imagecomp}
              source={{ uri: this.props.image }}
            />
          )}
          {this.props.svg && (
            <Image
              tintColor={
                this.props.disabled == true || this.props.active
                  ? "white"
                  : color.blueactive
              }
              style={styles.imagecomp}
              source={this.props.image}
            />
          )}
          {this.props.innerText && (
            <Text
              numberOfLines={2}
              style={{
                width: hp(this.props.width),
                textAlign: "center",
                marginTop: 4,
                fontSize: hp("1.9%"),
                color:
                  (this.props.innerText && color.white) ||
                  (this.props.active ? color.blueactive : color.black)
              }}
            >
              {this.props.text}
            </Text>
          )}
        </TouchableOpacity>
        {!this.props.innerText && (
          <Text
            numberOfLines={2}
            style={{
              width: hp(this.props.width),
              textAlign: "center",
              marginTop: 4,
              fontSize: hp("1.9%"),
              color: this.props.active ? color.blueactive : color.black
            }}
          >
            {this.props.text}
          </Text>
        )}
        {this.props.circleCount && (
          <View style={[styles.circleCount, this.circleCountStyles()]}>
            <Text
              style={{
                color: "black",
                textAlign: "center",
                fontSize: hp("1.5%")
              }}
            >
              {this.props.circleCount}
            </Text>
          </View>
        )}
      </View>
    );
  }
} 



const styles = StyleSheet.create({
  circleMain: {
    alignItems: "flex-start",
    paddingBottom: 25,
    justifyContent: "space-around",
    width: "100%",
    flexDirection: "column"
  },
  circle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1.5,
    borderColor: "orange"
  },
  activecolor: {
    backgroundColor: color.top_red
  },
  imagecomp: {
    width: "60%",
    height: "60%"
  },
  circleCount: {
    position: "absolute",
    top: -4,
    right: -4,
    backgroundColor: "white",
    borderWidth: 1,
    alignItems: "center"
  },
  disablecircle:{
    backgroundColor:'rgba(66,66,62,0.5)',
    borderColor:'transparent',

  }
});


export default CircleText;
import React, { Component } from 'react';
import {Text} from 'react-native';
import CountryPicker from 'react-native-country-picker-modal';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

// change the import path according to your project structure
// import closeImgLight from "/asset/iconWhite.png";
import {StyleSheet} from 'react-native';

const DARK_COLOR = "rgba(0,0,0,0.7)";
const PLACEHOLDER_COLOR = "#fff";
const LIGHT_COLOR = "#FFF";

var self="";
export default class CountryList extends Component {

  constructor(props){
    super(props);
    self=this;
    this.state={
      code:'IN',
      callingCode:'91'
    }
  }

  onChange=(data)=>{
   // alert(JSON.stringify(data))
    self.setState({code:data.cca2,callingCode:data.callingCode });
    self.props.mycode(data);
  }
  render(){
    return(
      <CountryPicker
  cca2={this.state.code}
  filterable={true}
  closeable={true}
  autoFocusFilter={true}
  transparent={true}
  showCallingCode={true}
  onChange={this.onChange}
    filterPlaceholderTextColor={PLACEHOLDER_COLOR}
    // closeButtonImage={require('../src/img/closewhite.png')}
    styles={darkTheme}
  >
  <Text style={{fontSize:hp('2.3%'),color:'black',paddingLeft:5,paddingRight:5}}>+{this.state.callingCode}</Text>
  </CountryPicker>
      )
  }
  componentDidMount(){
    this.setState({code:'IN'})
  }
  
};


const darkTheme = StyleSheet.create({
 modalContainer: {
    backgroundColor: DARK_COLOR
  },
  contentContainer: {
    backgroundColor: DARK_COLOR
  },
  header: {
    backgroundColor: DARK_COLOR
  },
  itemCountryName: {
    borderBottomWidth: 0
  },
  countryName: {
    color: LIGHT_COLOR
  },
  letterText: {
    color: LIGHT_COLOR
  },
  input: {
    color: LIGHT_COLOR,
    borderBottomWidth: 0,
    borderColor: LIGHT_COLOR
  }
});
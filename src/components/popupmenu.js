import React,{Component} from "react";
import { Keyboard,Text, View,TouchableOpacity,ScrollView } from "react-native";
import Menu, { MenuItem, MenuDivider, Position } from "react-native-enhanced-popup-menu";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
 import {Left,List,Container,Icon,Button,Subtitle,Header,Content,Body,Right,Title,Item,Input, ListItem, CheckBox,Accordion } from 'native-base';
import color from '../Helpers/color';
var extraoffset={
  top: -160,
  left: 0,
  bottom: 0,
  right: 0,
}
class PopupMenu extends  Component{
  // alert(JSON.stringify(props));
  constructor(props) {
    super(props);
    this.state = {textRef:React.createRef(),menuRef : null,keyboardshow:false,noextraoffset:{left:0,top:hp(props.height)-10}};
  }
  componentWillReceiveProps=(props)=>{
    this.setState({keyboardshow:props.keyboardshow})
  }

   setMenuRef=(ref)=>{
     this.state.menuRef = ref;
   };
  // const hideMenu = (data) => menuRef.hide();
  hideMenu=(data)=>{
    // alert(JSON.stringify(data));
     this.state.menuRef.hide()
     this.props.sendpopupobj(data);
    // alert(JSON.stringify(data));
  }
  // alert(textRef.current)
   
  showMenu = () => {

    this.state.menuRef.show(this.state.textRef.current, stickTo = Position.LEFT_TOP,this.props.showextraoffset?extraoffset:this.state.noextraoffset);
  }
 
  onPress = () =>{
   if(this.state.keyboardshow==true){
Keyboard.dismiss();
   }else{
  this.showMenu();
   }
    }

 render(){
  return (
    <View style={{ width:wp(this.props.width),
    height:hp(this.props.height),flexDirection:'row', alignItems: "center", backgroundColor: "white",borderWidth:0.8,borderColor:color.black1}}>
      
 
      <TouchableOpacity
      ref={this.state.textRef}
      style={{paddingLeft:'5%',width:'100%'}}
        title="Show menu"
        onPress={this.onPress}
      ><Text
        numberOfLines={1}
        style={{ width:'75%',fontSize: this.props.textfont?this.props.textfont: hp('2%'), textAlign: "left" }}
       >
        {this.props.activeobj?this.props.activeobj.name:''}
       </Text>
       <Text style={{position:'absolute',right:5}}> <Icon style={{color:color.black1,fontSize:hp('2.3%'),marginLeft:12}} type="FontAwesome" name='angle-down'/></Text>
       </TouchableOpacity>
      <Menu
        ref={this.setMenuRef}
        style={{width:wp(this.props.width),borderRadius:0,borderLeftWidth:0.2,borderColor:color.black1}}
    
        
      >
 <ScrollView style={{height:this.props.showextraoffset?150:195}}>
      {this.props.data.map((obj,key)=>{
return(
  <View style={{paddingLeft:5,paddingRight:5}}>
        <MenuItem key={key}  textStyle={[styles.menuitem,{paddingLeft:10,fontSize:this.props.textfont?this.props.textfont: hp('2%')}]}  onPress={()=>this.hideMenu(obj)}>{obj.name}</MenuItem>
        {key!=this.props.data.length-1&&
        <MenuDivider color={color.black1} />
        }
        </View>
        )
      })}
 </ScrollView>
        
      </Menu>
    </View>
  );
}
};
 const styles={
   menuitem:{
     paddingLeft:0,
   }
 }
export default PopupMenu;
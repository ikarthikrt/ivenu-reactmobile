import React,{Component} from 'react'
import {View,StyleSheet,Text,ScrollView,Image,TouchableOpacity} from 'react-native'
import {Content} from 'native-base'
import color from '../Helpers/color';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ListingDetails from '../pages/ListingDetails';
import Line from '../components/Line';
import Tick_mark from '../components/TickComp'

export default class Home_venuelist extends Component{
    constructor(props) {
      super(props);
    
      this.state = {activeCategory:null,suggestiondata:props.suggestiondata?props.suggestiondata:[]};
    }
    componentWillReceiveProps(props){
         if(props.visiblescreenData){
        if(props.visiblescreenData!='homevenue'){
          var suggestiondata=this.state.suggestiondata;
          suggestiondata.map((obj)=>obj.visible=false);
          this.setState({activeCategory:null,suggestiondata});
        }
        // return;
      }
        if(props.suggestiondata){
            this.setState({suggestiondata:props.suggestiondata})
        }
    }
clearActiveCategory=()=>{
      var suggestiondata=this.props.suggestiondata;
    suggestiondata.map((obj)=>obj.visible=false);
    this.setState({suggestiondata,activeCategory:null});
}
  changeItemSlider=(item,index,visible)=>{
    this.props.visiblescreen&&this.props.visiblescreen('homevenue');
    var suggestiondata=this.state.suggestiondata;
    suggestiondata.map((obj)=>obj.visible=false);
    suggestiondata[index].visible=visible?!visible:true;
    this.setState({suggestiondata});
    if(suggestiondata[index].visible==true){
    this.setState({activeCategory:item})

    }else{
      this.setState({activeCategory:null})
    }

  }
    render(){
        return(
            <View style={styles.container}>

                <ScrollView horizontal={true}
                   showsHorizontalScrollIndicator={false}	>

                    {this.state.suggestiondata&&this.state.suggestiondata.map((obj,index)=>
                    {
                        return(

                        <TouchableOpacity style={styles.square}  onPress={()=>this.changeItemSlider(obj,index,obj.visible?obj.visible:null)}>
                            <Image style={styles.image_style} source= {{uri:obj.photos.length>0&&obj.photos[0].venue_image_path}}>
                                   
                            </Image>
                            <Text style={styles.text_style}>
                                {obj.trn_venue_name}
                            </Text>  
                              {obj.visible==true &&                                  
                             <View style={{position:'absolute',bottom:0,top:hp('4%'),alignItems:'center',height:'100%',left:hp('1%'),zIndex:1}}>
                        
                           
                        <Tick_mark/>
                            
 
                            </View>
                        }
                        </TouchableOpacity>

                            )
                    })
                    }

                </ScrollView>
             {this.state.activeCategory&&
                    <ListingDetails activeCategory={this.state.activeCategory}  clearList={()=>this.clearActiveCategory()}>
                    <Text>Cancel</Text>
                    </ListingDetails>
                }
            </View>
  
)
    }
}
const styles=StyleSheet.create({
    container:{
        flex:1
    },
    square:{
        margin:5,
        width:hp('15%'),
        
    },
    image_style:{     
        height:hp('10%'),
        width:hp('15%'),
        borderRadius:5,
        justifyContent:'center',
            
    },
    text_style:{
      //  textAlign:"center",
        fontSize:hp('1.6%'),
    },
  
})
import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,ScrollView,FlatList,} from 'react-native';
import {} from 'native-base'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import Tick_mark from '../images/tick.png'

export default class Tickcomp extends Component{
    render(){
        return(
            <View style={styles.container}>
                <TouchableOpacity>
                    <Image source={Tick_mark}
                            style={styles.tick_style}>

                    </Image>
                </TouchableOpacity>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        height:hp('4%'),
        width:hp('4%'),
        borderRadius:hp('5%')/2,
        backgroundColor:color.white,
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        padding:5
    },
    tick_style:{
        width:hp('2.5%'),
        height:hp('2.5%'),
        padding:5
    }
})
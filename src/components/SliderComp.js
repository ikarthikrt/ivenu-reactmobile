import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,ScrollView,Dimensions} from 'react-native';
import {Actions,Router,Scene} from 'react-native-router-flux'
import {Right,Top,Icon, Button} from 'native-base'
import color from '../Helpers/color'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ModalDropdown  from 'react-native-modal-dropdown'
import TopComp from '../components/TopComp'
import Home_venuelist from '../components/home_venuelist'

import White_arrow from '../images/white_dwn_arrow.png'
import Left_arrow from '../images/left.png'
import Right_arrow from '../images/right.png'
import Down_arrow from '../images/arrow_down.png'
import ADD_img from '../images/add_icon.png'

screenwidth= Dimensions.get('window').width

export default class SliderComp extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View>
                    <ScrollView
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        pagingEnabled={true}
                        ref = {re => this.scroll = re}>
                        {this.props.slider_List&&this.props.slider_List.map((obj,key)=>
                            {
                        return(
                         <View>     
                            <Text style={styles.txt1}>
                            {obj.club_name}
                            
                            </Text>
                            <View style={styles.flex_row}>
                                <View style={{paddingTop:hp('1.5%'),width:wp('40%')}}>
                                    <Image source={obj.image} style={{height:hp('15%'),width:'100%'}}>

                                    </Image>
                                </View>
                                <View style={{backgroundColor:color.white,width:wp('60%')}}>
                                    <View style={styles.txtcontainer}>

                                        <Text style={{fontSize:hp('1.5%'),}}>{obj.price.length>0&&(obj.price[0].trn_venue_price_currency+" "+obj.price[0].trn_venue_price_amt)}</Text>
                                        
                                        <Text style={{fontSize:hp('1.5%'),}}
                                        numberOfLines={5}>
                                        {obj.text}
                                       
                                        </Text>
                                    </View>                           
                                </View>

                            </View>
                        </View>
                          )
                        })
                        }
                    </ScrollView>
                     </View> 
                    <View style={{flexDirection:'row',position:'absolute', bottom:0,}}>
                        <View style={{width:wp('40%')}}></View>
                        <View style={{width:wp('60%'),flexDirection:'row',}}>
                            <View style={[{flex:1,paddingLeft:hp('1.5%'),flexDirection:'row'}]}>
                                <TouchableOpacity style={styles.view22}>
                                    <Image backgroundColor={color.white} source={Left_arrow} style={[styles.blue_arr,styles.view22]}>

                                    </Image>
                                </TouchableOpacity >
                                
                                <Text style={[{fontSize:hp('1.5%'),},styles._end,styles.view22]}>
                                        01 of 05
                                </Text>
                                <TouchableOpacity style={styles.view22} onPress={() => { this.scroll.scrollTo({ x: screenwidth })}}>
                                    <Image backgroundColor={color.white} source={Right_arrow} style={[styles.blue_arr,]}>

                                    </Image>
                                </TouchableOpacity>
                            </View>   
                            <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end'}}>
                                <Button style={[styles.booK_nw_btn]}
                                >
                                    <Text style={[styles.booK_nw_txt]}>More Details</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        padding:hp('1.5%'),
        marginBottom:hp('1.5%'),
        backgroundColor:color.white
    },
    txt1:{
        fontSize:hp('2%')
    },
    flex_row:{
        flexDirection:'row',
        height:hp('15%')
    },
    txtcontainer:{
        flex:.9,
        paddingTop:hp('1.5%'),
        paddingLeft:hp('1.5%'),
        paddingBottom:hp('1.5%')
    },
    blue_arr:{
        height:hp('2.5%'),
        width:hp('2.5%'),
        justifyContent:'center',alignSelf:'center',alignContent:'center',alignItems:'center'
    },
    booK_nw_txt:{
        color:color.white,
        fontSize:hp('1.5%'),
        paddingLeft:8,
        paddingRight:8
    },
    _end:{
        justifyContent:'flex-end',
        alignItems:'flex-end',
        alignSelf:'flex-end',
        alignContent:'flex-end',
    },
    booK_nw_btn:{
        backgroundColor:color.blue,
        height:hp('3%'),
        marginRight:hp('1.5%'),
        paddingLeft:8,
        paddingRight:8,
    },
    view22:{
        justifyContent:'center',alignSelf:'center',alignContent:'center',alignItems:'center'
    },
})
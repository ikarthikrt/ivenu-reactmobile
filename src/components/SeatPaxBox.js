'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  Text
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import placeholderImg from '../images/placeholder_img.png';
import color from '../Helpers/color';
import {
Accordion,
Container,
Content,
List,
ListItem,
Thumbnail,
Left,
Body,
Right,
Button,
Icon,
Footer,
FooterTab
} from "native-base"; 
 const screenWidth = Dimensions.get('window').width-50;
 var self="";
class SeatPaxBox extends Component {
constructor(props) {
  super(props);
self=this;
  this.state = {activeSlide:0};
}
checkImageURL=(url)=> {
  if(url.split('.').length>0){
  var imageurl=url.split('.')[url.split('.').length-1].toLowerCase();
  return ['jpg','jpeg','png','gif'].includes(imageurl);
}else{
  return false;
}
    // return(imageurl.match(/\.(jpeg|JPEG|jpg|JPG|gif|png|PNG)$/) != null);

}
renderPrice=(obj1)=>{
  if(this.props.type==3){
  return(
    
  <React.Fragment>
        {obj1.hour_cost>0&&
        <View style={styles.priceboxborder}>
        <Text>Hourly</Text>
        <Text>{obj1.hour_cost} ({obj1.currency})</Text>
        </View>
        }
        {obj1.day_cost>0&&
        <View style={styles.priceboxborder}>
        <Text>Daily</Text>
        <Text>{obj1.day_cost} ({obj1.currency})</Text>
        </View>
        }
        {obj1.week_cost>0&&
        <View style={styles.priceboxborder}>
        <Text>Weekly</Text>
        <Text>{obj1.week_cost} ({obj1.currency})</Text>
        </View>
        }
        {obj1.month_cost>0&&
        <View style={styles.priceboxborder}>
        <Text>Monthly</Text>
        <Text>{obj1.month_cost} ({obj1.currency})</Text>
        </View>
        }
      </React.Fragment>
    )
    }else{
      return(
      <React.Fragment>
       <View style={[styles.priceboxborder,{width:'50%'}]}>
        <Text>Adult</Text>
        <Text>{obj1.Adult} ({obj1.currency})</Text>
        </View>
        <View style={[styles.priceboxborder,{width:'50%'}]}>
        <Text>Child</Text>
        <Text>{obj1.Child} ({obj1.currency})</Text>
        </View>
      </React.Fragment>
      )

    }
}
_renderItem =({item, index})=> {

        return (
               <View style={styles.mainPricebox}>
               <Text style={styles.mainPricebox_text}>{this.props.type==3?item.seat_name:item.venue_pax_name}</Text>
               <View style={styles.priceBox}>
               {item.priceDetails.length>0&&item.priceDetails.map((obj)=>{
                if(this.props.type==2){
                  return(
                  <View style={{width:'96%'}}>
                    <Text style={{color:color.black,fontWeight:'bold',marginBottom:12}}>{obj.day_type_name}</Text>
                    <View style={{flexDirection:'row',justifyContent: 'space-between'}}>
                    {this.renderPrice(obj)}
                    </View>
                  </View>
                  )
                }else{
                  return this.renderPrice(obj);
                }
               })}
               </View>
                 <Button onPress={()=>this.props.sendSeatData&&this.props.sendSeatData(item)} primary style={{flexDirection:'row',justifyContent:'center'}}><Text style={styles.booktext}>Book Now</Text></Button>
               </View>
        );
    }
    get pagination () {
        const {  activeSlide } = this.state;
        return (
            <Pagination
              dotsLength={this.props.items.length}
              activeDotIndex={activeSlide}
              containerStyle={{ backgroundColor: 'transparent',position:'absolute',bottom:-12,left:-5 }}
              dotStyle={{
                  width: 10,
                  height: 10,
                  borderRadius: 5,
                  marginHorizontal: 8,
                  backgroundColor: 'rgba(0, 0, 0, 0.92)'
              }}
              inactiveDotStyle={{
                  // Define styles for inactive dots here
              }}
              inactiveDotOpacity={0.4}
              inactiveDotScale={0.6}
            />
        );
    }
  render() {
    // alert(JSON.stringify(this.props.items));
    return (
              <View style={{flex:1,marginTop:12,flexDirection:'row'}}>
              <TouchableOpacity style={[styles.fontarrowiconLeft]} onPress={() => this._carousel.snapToPrev()}>
              <Icon style={[styles.fontAwesomImage,{opacity:this.state.activeSlide==0?0.5:1}]}  type="FontAwesome" name='angle-left'/></TouchableOpacity>


              <Carousel
              ref={(c) => { this._carousel = c; }}
              data={this.props.items}
              firstItem={0}
              autoplay={false}
              loop={false}
               onSnapToItem={(index) => this.setState({ activeSlide: index }) }
              renderItem={this._renderItem}
              sliderWidth={screenWidth}
              itemWidth={screenWidth}
              itemHeight={null}
              inactiveSlideScale={1}
              slideStyle={{height:null}}
            />
              <TouchableOpacity style={styles.fontarrowiconRight} onPress={() => this._carousel.snapToNext()}>
              <Icon style={[styles.fontAwesomImage,{opacity:this.state.activeSlide==this.props.items.length-1?0.5:1}]}  type="FontAwesome" name='angle-right'/></TouchableOpacity>
                           </View>

        
    );
  }
}

const styles = StyleSheet.create({
    homeBgImage:{
        width:screenWidth,
      zIndex: 0
    },
    fontarrowiconRight:{
      top:0,
      bottom:0,
      right:0,
      height:'100%',
      alignItems:'center',
      width:'auto',
      zIndex:1,
      flexDirection:'row',
      paddingLeft:5

    
    }
    ,fontarrowiconLeft:{
      top:0,
      bottom:0,
     height:'100%',
      alignItems:'center',
      width:'auto',
      zIndex:1,
      flexDirection:'row',paddingRight:5
    },
    fontAwesomImage:{
      fontSize:hp('5%'),
      padding:5,
      color:color.black1
    },
    mainPricebox:{
      borderWidth:1,
      borderColor:color.black1,
      padding:12,
      margin:12,
    },
    mainPricebox_text:{
      fontSize:hp('2.2%'),
      textTransform:'capitalize',
      marginBottom:12,
    },
    priceboxborder:{
      borderWidth:1,
      borderColor:color.black1,
      padding:12,
      alignItems:'center',
      marginRight:12,
      marginBottom:8,

    },
    priceBox:{
      flexDirection:'row',
      flexWrap:'wrap'
    },
    booktext:{
      textAlign:'center',
      color:color.white
    }
});


export default SeatPaxBox;
import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,TouchableOpacity
} from 'react-native';
import { Container, Header, Content, Card, CardItem, Body,Subtitle,Icon } from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
class BookingCard extends Component {
  render() {
    return (
      <TouchableOpacity onPress={()=>this.props.onClick&&this.props.onClick()}>
       <Card>
             {this.props.backarrow==true&&
        <CardItem>
            <TouchableOpacity onPress={()=>this.props.goback&&this.props.goback()}><View style={{flexDirection:'row',alignItems:'center'}}><Icon style={{color:color.black1,fontSize:hp('2.5%')}} type="FontAwesome" name="chevron-left"/><Text style={{textAlign:'left',fontSize:hp('2.5%'),marging:0,padding:0,marginLeft:-12}}>Back</Text></View></TouchableOpacity>
       		 </CardItem>
       		 }
            <CardItem header>
            <View style={{flex:1,justifyContent:'space-between',flexDirection:'row'}}>
            <View style={{flex:0.5}}>
           
            <View style={{flexDirection:'row'}}>
              <Text numberOfLines={2} style={styles.leftHeader}>{this.props.leftHeader}</Text>
              </View>
              <Text numberOfLines={1} style={[styles.leftHeader,{fontSize:hp('2%'),color:color.blueactive}]}>{this.props.subtitle} Booking</Text>
              </View>
              <Text numberOfLines={1} style={styles.rightHeader}>{this.props.rightHeader}</Text>
              </View>
            </CardItem>
            <CardItem>
              <View style={{flex:1}}>
             	{this.props.children}
              </View>
            </CardItem>
         </Card>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
leftHeader:{
	textAlign:'left',
	fontSize:hp('2.3%'),
	color:color.orangeBtn,
	fontWeight:'bold'
},rightHeader:{
	flex:0.5,
	textAlign:'right',
	fontSize:hp('2.1%'),
	fontWeight:'bold'
}
});


export default BookingCard;
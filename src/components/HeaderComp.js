import React, { Component } from 'react';
import {View,Image,StatusBar,Picker,Text,TouchableOpacity,StyleSheet,PermissionsAndroid} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Geolocation from 'react-native-geolocation-service';
import color from '../Helpers/color'
import logo from '../images/logo.png'
import search from '../images/Search.png'
import meanu from '../images/menu.png'
import set_pwd from '../images/set_pwd.png'
import { Actions,Router} from 'react-native-router-flux'
import Modeldropdowncomp from '../components/ModeldropdownComp'
import Down_arrow from '../images/orange_arow_down.png'
import AsyncStorage from '@react-native-community/async-storage';
import Search_your_venue from '../pages/Search_your_venue';
import Triangle from './triangles';
import ModalComp from '../components/Modal';
import AddressComp from "../components/addressComp"; 
import {Icon} from 'native-base';
import CommonSearch from '../pages/commonsearch';
import Toast from 'react-native-simple-toast';
import links from '../Helpers/config';


var location=['California', 'NewYork',]

export default class HeaderComp extends Component {
    constructor(props){
      super(props);
      this.state={
        manualaddress:null,
        selected:'key0',currentlocation:null,
        modalstate:false,
    chosenlocation:{latitude:0,longitude:0},
    address:'',search_venue:false,headervisible:false,modalsubtitle:null,title:null,visible:true,loadtoast:null,loading:null
      } 


      


    }
    onValueChange=(value)=>{
      this.setState({
        selected: value,loginDetails:null
      });
    }
    componentWillReceiveProps(props){
    //   AsyncStorage.getItem('loginDetails', (err, result) => {
      
    //    if(result!=null){
    // this.setState({loginDetails:JSON.parse(result)})
    //   }else{
    // // this.setState({loginDetails:null})

    //   }
    //  });
          }
    async getaccess(){
      const granted= await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,{})
      // console.log('granted',granted);
      if(granted===PermissionsAndroid.RESULTS.GRANTED){
       return true;
      }else{
        return false;
      }
    }
  async loadcurrentLocation(){


  this.setState({currentlocation:'blue'});

  // alert(granted);
  Geolocation.getCurrentPosition(
       async (position) => {
       
        //  alert(JSON.stringify(position));
              await AsyncStorage.setItem(
                "chosenlocation",
                JSON.stringify({
                  lat: position.coords.latitude,
                  lng: position.coords.longitude
                })
              );
         this.setState({chosenlocation:{latitude:position.coords.latitude,longitude:position.coords.longitude}});
        this.getAddressByLatLng(position.coords);
       },
       
       (error) => {
                // See error code charts below.
                console.log(error.code, error.message);
            },
       {timeout: 10000,maximumAge:0},
     );
}

    
async componentWillMount(){
  var self=this;
  var setIntervalData=setInterval(()=>{
 AsyncStorage.getItem('loginDetails', (err, result) => {
  
       if(result!=null){
         console.log(result);
         clearInterval(setIntervalData);
    self.setState({loginDetails:JSON.parse(result)})
      }
     });
  },3000);

      await AsyncStorage.getItem('chosenlocation', async (err, coordinates) => {
        // console.log('coordinates',coordinates);
        // alert(JSON.stringify(coordinates));
         if (coordinates == null || coordinates == "" || coordinates == undefined) {
           console.log("didmountcallapi");
           this.loadcurrentLocation();
         } else {
           await AsyncStorage.getItem("manualaddress", async(err, position) => {
             if (err) console.log("err", err);
             else if (position == null) {
               console.log('pos null')
               this.loadcurrentLocation();
             } else 
             {
               // this.setState({address})
               var getaddressData=await AsyncStorage.getItem("addressCity");
               if(getaddressData==null){
               this.getAddressByLatLng(JSON.parse(position));
               }else{
                 this.setState({address:getaddressData});
               }
             }
           });
         }

       }) 

// AsyncStorage.clear()
  
}
gotoHome=()=>{
  if(Actions.currentScene!='home'){
    // Actions.home();
    Actions.reset('home');
  }
}
findAddresss=(data)=>{
  var filteredRecords=[];
  for(var i=0;i<data.length;i++){
// findAddresss(index)
var filterRecords=data[i].address_components.filter(x => x.types.filter(t => t == 'sublocality_level_1').length > 0);
  // alert(JSON.stringify(filterRecords));
  console.log('filterRecords',filterRecords);
if(filterRecords.length>0){
  // this.findAddresss()
  // filteredRecords=
  // i=999999;
  return filterRecords;
  }else{
    filteredAddress=filterRecords;
     // [];
  }
}
if(filteredRecords.length==0){
  return [];
}

}
    receiveAddress = async (data) => {
        this.setState({ modalstate: false });
     if(!data){
       return;
     }
     console.log("positionl", data.position);
        //udpate to async storate 
       
       await AsyncStorage.setItem("chosenlocation", JSON.stringify(data.location));
       await AsyncStorage.setItem("manualaddress", JSON.stringify(data.position));
       // alert(JSON.stringify(data.position))

       if (Actions.currentScene == "home") {
         // Actions.home();
         this.getAddressByLatLng(data.position,function(data){
         Actions.reset("home");
         });
       }else{
        // this.forceUpdate()
        this.getAddressByLatLng(data.position,function(data){
        Actions.refresh({ key: Math.random() });
         });
       } 

      
    }

     
  getAddressByLatLng= async(data,callback)=>{
 
  fetch("https://maps.googleapis.com/maps/api/geocode/json?latlng="+data.latitude+","+data.longitude+"&key="+links.GLINK).then((response)=>response.json()).then(async (responseJson)=>{

  var address=null;
  // alert(responseJson.results.length);
    if(responseJson.results.length==0){
      address="No Area Found";
      await AsyncStorage.setItem('addressCity',address);
   this.setState({address:address});
   callback&&callback(true);

            return;
          } 
          
   let filteredAddress= this.findAddresss(responseJson.results);
 

   this.setState({
     address:
       filteredAddress.length > 0
         ? filteredAddress[0].long_name
         : "No Area Found"
   },async function(){
      await AsyncStorage.setItem('addressCity',this.state.address);
callback&&callback(true);
   });
  })
}
    openDrawer=()=>{
      // AsyncStorage.getItem('loginDetails', (err, result) => {
      // if(result!=null){
      Actions.drawerOpen();
      // }else{
      // this.setState({sessionTrue:false});
      // }
      // });
    }
   async componentDidMount(){
      

  }
  addVenue=()=>{
    if(Actions.currentScene=='venuepage' || Actions.currentScene=='venueform' || Actions.currentScene=='corporateform' ){

    }else{
    Actions.venuepage();
    }
  }
  searchDisplay=()=>{
    this.setState({search_venue:true})
  }
  closemodal=()=>{
            this.setState({search_venue:false})
        if(this.props.closetabmodal){
            this.props.closetabmodal();
        }
    }
    sendreceivedata=(data)=>{
       this.setState({search_venue:false})
       Actions.Venue_search({data:data,actionshide:false,latlng:this.state.chosenlocation});
    }
       nxtpage = (data, data1, data2,count) => {
     // alert(JSON.stringify(data));
     Actions.Search_Home({ data: data, data1: "data1", data2: data2,count:count });
   };
submitSearch = async (data) => {
     const manuallocation = JSON.parse(
       await AsyncStorage.getItem("chosenlocation")
     );
     let lat = manuallocation != null ? manuallocation.lat : null;
     let long = manuallocation != null ? manuallocation.lng : null;
     // this.setState({loading:true})
     if (data.search_string != "") {
       fetch(links.APIURL + "commonSearch", {
         method: "POST",
         headers: {
           Accept: "application/json",
           "Content-Type": "application/json"
         },
         body: JSON.stringify({
           searchContent: data.search_string,
           nearme: data.nearme,
           offset: 0,
           lat: lat,
           long: long
         })
       })
         .then(response => response.json())
         .then(responseJson => {
           var self=this;
     // setTimeout(()=>{
       this.setState({loading:'clear'})
     // },200)
  // alert(JSON.stringify(responseJson));
           console.log("search res", responseJson.data);
           // alert(responseJson.count);
           if (responseJson.status == 0&&responseJson.data.length>0) {
             this.setState({search_venue:false})
             Actions.Search_Home({ data: responseJson.data, data1: "data1", data2: null,count:responseJson.count,textstring:data.search_string,nearme:data.nearme });

            // Actions.nxtpge(responseJson.data, "", null,responseJson.count);
             // Actions.Venue_search({
             //   data: responseJson.data.length > 0 ? responseJson.data : [],
             //   data1: "",
             //   data2: "",
             //   offset:0,
             //   nearme:data.nearme,
             //   searchContent:data.search_string,
             //   latlng: {
             //     latitude: lat,
             //     longtitude: long
             //   }
             // });
           } else {
            Toast.show("No Records Found", Toast.LONG);
             this.setState({ NoRecords: true });
           }
         });
            
     } else
     {
      Toast.show("No Records Found", Toast.LONG);
    }
   };
    render() {

      console.log('rendered')
      return (
        <View style={styles.headerstyle}>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View
              style={{ flex: 0.7, flexDirection: "row", alignItems: "center" }}
            >
              <TouchableOpacity onPress={() => this.gotoHome()}>
                <Image source={logo} style={styles.headerIcon} />
              </TouchableOpacity>
              <View
                style={{
                  flex: 0.8,
                  flexDirection: "column",
                  paddingLeft: hp("3%")
                }}
              >
                {this.state.loginDetails && (
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      justifyContent: "flex-start",
                      paddingTop: 10
                    }}
                  >
                    {/*<Image style={{width:hp('2%'),height:hp('2%'),borderRadius:hp('2%')/2}} source={set_pwd}></Image>*/}
                    <Text style={{ fontSize: hp("1.6%") }}>Welcome</Text>
                    <Text style={{ color: color.red, fontSize: hp("1.6%") }}>
                      {" "}
                      {this.state.loginDetails.user_name}
                    </Text>
                  </View>
                )}

                <View
                  style={{
                    flexDirection: "row",
                    flex: 1,
                    alignItems: "center"
                  }}
                >
                  {/*} <View style={{flex:1,marginBottom:5}}>    
                                  <Modeldropdowncomp  
                                      height={5}
                                      drop_items={"California"}
                                      values={location} /> 
                              </View> 
                              <TouchableOpacity style={{flex:1,}}>
                                  <Image source={Down_arrow} 
                                          style={styles.drop_down_top}>
                                  </Image>
                              </TouchableOpacity>    */}
                  <View>
                    <Text onPress={() => this.setState({ modalstate: true })}>
                      {this.state.address ? this.state.address : "Loading..."}
                    </Text>
                  </View>
                </View>
              </View>
            </View>

            <View
              style={{
                flex: 0.3,
                flexDirection: "column",
                alignItems: "flex-end"
              }}
            >
              <View style={{ flex: 5, justifyContent: "center" }}>
                <TouchableOpacity onPress={() => this.addVenue()}>
                  <Text
                  numberOfLines={1}
                    style={{
                      fontSize: hp("2%"),
                      justifyContent: "center",
                      color: color.top_red
                    }}
                  >
                    Add Your <Text style={{ fontWeight: "bold" }}>Venue</Text>
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flex: 5,
                  flexDirection: "row",
                  height: "100%",
                  alignItems: "center",
                  justifyContent: "flex-end",
                  marginBottom: 8
                }}
              >
               <TouchableOpacity
                  onPress={() => this.searchDisplay()}
                  transparent
                  style={styles.buttonRight}
                >
                  <Icon type="FontAwesome" name="search" style={{color:color.orangeDark,fontSize:hp('3%')}}/> 
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.openDrawer()}
                  transparent
                  style={[styles.buttonRight,{marginLeft:hp('1.5%')}]}
                >
                  <Image source={meanu} style={styles.icons_style} />
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View style={{ position: "absolute", bottom: 0 }}>
            {this.state.search_venue == true && (
              <View>
                {/*<View style={{alignItems: 'center', justifyContent: 'flex-end',flexDirection:'row',marginLeft:10,width:'100%'}}>
                    
            <View style={{position:'absolute',top:-hp('3%'),right:hp('9%'),zIndex:9999}}>
            <Triangle  width={hp('3.6%')} height={hp('3.6%')}  color={color.white} direction={'down-right'}>
            </Triangle>
       </View><View style={{position:'absolute',top:-hp('3%'),right:hp('9%'),zIndex:9999}}>
            <Triangle  width={hp('2.9%')} height={hp('2.9%')}  color={color.gray} direction={'down-right'}>
            </Triangle>
       </View>
       </View>*/}
                <ModalComp
                  header={this.state.headervisible}
                  borderclr={color.grey}
                  borderwdth={2}
                  Nooverlay={true}
                  center={true}
                  bgcolor={color.white}
                  titlecolor={color.orange}
                  closeiconcolor={color.grey}
                  closemodal={this.closemodal}
                  modaltitle={this.state.title}
                  modalsubtitle={this.state.modalsubtitle}
                  visible={this.state.visible}
                >
                  <CommonSearch loading={this.state.loading} sendsearch={(data)=>this.submitSearch(data)}/>
                 {/* <Search_your_venue
                    cancelSearch={this.closemodal}
                    sendsearchdata={data => this.sendreceivedata(data)}
                  />*/}
                </ModalComp>
              </View>
            )}
          </View>
       
         
              <AddressComp
                receiveAddress={this.receiveAddress}
                modalstate={this.state.modalstate}
                onClose={()=>this.setState({modalstate:false})}
              />
          
         
        </View>
      );
    }
  }
  const styles=StyleSheet.create({
    headerstyle:{
      height:hp('10%'),
      paddingLeft:12,
      paddingRight:12,
      backgroundColor:color.white
    },
    buttonRight:{
      marginLeft:hp('3%') 
    },
    
    headerIcon:{
         width:hp('11%'),
      height:hp('5.15%'),
    },
    icons_style:{
      width:hp('3%'),
      height:hp('3%'),
    },
    drop_down_top:{
      height:hp('1.5%'),
      width:hp('1.5%'),
    }
})
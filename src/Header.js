import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import {View,Image,StatusBar,Picker,Text,TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Search from '../images/svg/search.png';
import Menu from '../images/svg/Menu.png';
import colors from '../helpers/colors';
import { Actions} from 'react-native-router-flux'
import HeaderLogo from '../images/svg/Header_logo.png';
import TabLoginProcess from './TabLoginProcess';
import AsyncStorage from '@react-native-community/async-storage';
export default class HeaderComp extends Component {
  constructor(props){
    super(props);
    console.log(props);
    this.state={
      selected:'key0',
      sessionTrue:null
    }
  }
  componentWillReceiveProps(props){
    console.log(props);
  }
  onValueChange=(value)=>{
    this.setState({
      selected: value
    });
    AsyncStorage.setItem('value',value);

  }
  closetabmodal=(data)=>{
    // alert("hiii");
    this.setState({sessionTrue:null});
  }
  openDrawer=()=>{
    AsyncStorage.getItem('loginDetails', (err, result) => {
      if(result!=null){
    Actions.drawerOpen();
      }else{
    this.setState({sessionTrue:false});

      }
    });
  }
  async componentWillMount(){
    const value = await AsyncStorage.getItem('value');
    // alert(value);
    if(value!=null){
      this.setState({
      selected: value
    });
    }
  }
  tabAction=()=>{
    // alert('hii');
  }
  render() {
    // alert("h");
    return (
        <View style={[styles.headerstyle,{backgroundColor:colors.white}]}>
           <StatusBar
     backgroundColor={this.props.statuscolor}
     barStyle="light-content"
   />
  <View style={{flex:1,flexDirection:'row'}}>
  <View style={{flex:0.8,flexDirection:'row',alignItems:'flex-end',marginBottom:8}}>
   <Image source={HeaderLogo} style={styles.headerIcon}/>
  <View style={{flex:0.8,flexDirection:'row',alignItems:"center",justifyContent:'flex-end',marginLeft:6,height:'36%'}}>
  <Picker 
              mode="dropdown"
              style={{ width:'100%',alignItems:'flex-end'}}
              selectedValue={this.state.selected}
              onValueChange={this.onValueChange.bind(this)}
            >
              <Picker.Item label="California" value="key0" />
              <Picker.Item label="NewYork" value="key1" />
         
            </Picker>
           
            </View>
  </View>
  <View style={{flex:0.2,flexDirection:'row',alignItems:'flex-end'}}>
  <View style={{flex:1,flexDirection:'row',height:'100%',alignItems:'center',justifyContent:'flex-end'}}>
            <TouchableOpacity transparent style={styles.buttonRight} >
            <Image source={Search}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.openDrawer()} transparent style={styles.buttonRight}>
            <Image source={Menu}/>
            </TouchableOpacity>
            </View>
  </View>
{this.state.sessionTrue==false&&
        <TabLoginProcess type='login' visible={true} tabAction={this.tabAction} closetabmodal={this.closetabmodal}/>
      }
  </View>
        </View>
    );
  }
}
const styles={
  headerstyle:{
    height:hp('8%'),
    paddingLeft:12,
    paddingRight:12
  },
  buttonRight:{
    marginLeft:hp('3%')
  },
  headerIcon:{
    width:hp('15%'),
    height:hp('5%'),

  }
}
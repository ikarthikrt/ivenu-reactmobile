import React, {Component} from 'react';
import {ScrollView,Dimensions,TextInput,Platform,Text, StyleSheet, View,Alert,StatusBar,Modal,Image,TouchableHighlight,TouchableOpacity,Picker} from 'react-native';
import {Left,List,Container,Icon,Button,Subtitle,Header,Content,Body,Right,Title,Item,Input, ListItem, CheckBox,Accordion } from 'native-base';
import color from '../Helpers/color';
const DropDown = require('react-native-dropdown');

const {
  Select,
  Option,
  OptionList,
  updatePosition
} = DropDown;
const width=Dimensions.get('window').width;
const height=Dimensions.get('window').height;
var customdropdowns=[{id:1,name:'USD'},{id:2,name:'INR'}]
export default class CustomDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      canada: '',
      dropdown:props.dropdown,
      dropdownData:props.data
    };
  }
  componentWillReceiveProps=(props)=>{
    var dropdown=this.state.dropdown;
    dropdown.x=props.dropdown.x;
    dropdown.y=props.dropdown.y;
    dropdown.height=props.dropdown.height
    dropdown.width=props.dropdown.width
    dropdown.mainheight=props.mainheight
    this.setState({dropdown});
  }

  componentDidMount() {
    updatePosition(this.refs['SELECT1']);
    updatePosition(this.refs['OPTIONLIST']);
  }

  _getOptionList() {
    return this.refs['OPTIONLIST'];
  }
  changeList=(data)=>{
    this.props.changeData(data);
    // alert(JSON.stringify(data));
  }

  
  _canada(province) {

  this.setState({
      ...this.state,
      canada: province
    });
  }

  render() {
    var topdropdownoffset=(this.state.dropdown.y-(height-this.props.mainheight))+this.state.dropdown.height;
    var bottomdropdownoffset=this.props.mainheight-topdropdownoffset;
    var toporbottom=this.props.top?{"top":topdropdownoffset}:{"bottom":bottomdropdownoffset};
    return (
      <View style={{position:'absolute',bottom:0,flex:1,zIndex:9999,height:height,width:width}}>
      <ScrollView style={[{elevation:24,zIndex:9999999,width:this.state.dropdown.width,height:'auto',borderWidth:0,borderColor:color.black1,flex:1,position:'absolute',left:this.state.dropdown.x-8},toporbottom]}>
      {this.state.dropdownData.map((obj,key)=>{
return(
          <TouchableOpacity onPress={()=>this.changeList(obj)} key={key} activeOpacity={1} style={[styles.listitem]}><Text>{obj.name}</Text></TouchableOpacity>
          )
      })
    }
   
      </ScrollView>
   </View>
    );
  }
}
const styles={
  listitem:{
    padding:12,
    backgroundColor:'white',
    borderWidth:0,
    borderBottomWidth:1,
    borderColor:color.black1,
  },
  active:{
    backgroundColor:color.blueactive
  },
  last:{
     padding:12,
    borderBottomWidth:1
  }
}
import React from "react";
import { GiftedChat, Bubble } from "react-native-gifted-chat";
import color from "../../Helpers/color";
import moment from "moment";

export default class ChatView extends React.Component {
                 state = {
                   messages: []
                 };

                 componentDidMount() {
                   this.setState({
                     messages: []
                   });
                 }

                 componentWillReceiveProps(props) {
                   if (props.messages != this.state.messages) {
                     var obj = [];
                     props.messages.forEach(element => {
                       var temp = {};
                       temp._id = element.chat_senderId;
                       temp.text = element.text;
                       temp.createdAt = element.created_at;
                       temp.user = {
                         _id: element.chat_receiverId,
                       };
                       obj.push(temp);
                     });
                     this.setState({
                       messages: obj
                     });
                   }

                   if(props.clear){
                     console.log('clear called ')
                       clearInterval(this.timer);
                   }
                 }

                 onSend(messages = []) {
                  //  this.setState(previousState => ({
                  //    messages: GiftedChat.append(
                  //      previousState.messages,
                  //      messages
                  //    )
                  //  }))

                    var obj = {
                      chatMessage: messages[0].text,
                      chatReceiverId:
                        this.props.userId.customerUserId != null
                          ? this.props.userId.customerUserId
                          : this.props.userId.venue_user_id,
                      messageTime: moment().format("YYYY-MM-DD hh:mm:ss"),
                      venueId: this.props.userId.venue_id
                    };
                    this.props.pushMessage(obj);
                 }

                 renderBubble(props) {
                   return (
                     <Bubble
                       {...props}
                       wrapperStyle={{
                         right: {
                           backgroundColor: color.orange
                         },
                         left: {
                           backgroundColor: color.white
                         }
                       }}
                     />
                   );
                 }


                 componentDidMount = () => {
                   console.log('didmount') 
                   this.timer = setInterval(() => {
                     this.props.refresh()
                   }, 5000);
                 };

                 componentDidUpdate = () => {
                   console.log('did update')
                 };
                 

                 componentWillUnmount() {
                    console.log("willunmount");
                   clearInterval(this.timer);
                 }

                 render() {
                  
                   return (
                     <GiftedChat
                       messages={this.state.messages}
                       onSend={messages => this.onSend(messages)}
                       alwaysShowSend={true}
                       renderBubble={this.renderBubble}
                       alignTop={true}
                       scrollToBottom={true}
                       user={{
                         _id:
                           this.props.userId.customerUserId != null
                             ? this.props.userId.customerUserId
                             : this.props.userId.venue_user_id
                       }}
                     />
                   );
                 }
               }

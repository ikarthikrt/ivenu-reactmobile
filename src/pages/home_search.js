import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,Alert,FlatList,ScrollView,ActivityIndicator,PermissionsAndroid} from 'react-native';
import {Actions,Router,Scene} from 'react-native-router-flux'
import {Right,Top,Icon, Button,Content} from 'native-base'
import color from '../Helpers/color'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ModalDropdown  from 'react-native-modal-dropdown'
import CircleComp from '../components/circlecomp'
import Dropdown_container from '../components/drpdown_container'
import Right_arrow from '../images/rightarrow.png'
import Modeldropdowncomp from '../components/ModeldropdownComp'
import links from '../Helpers/config';
import GroundNearYou from './SearchListingSlider';
import Toast from 'react-native-simple-toast';
import LoadingOverlay from '../components/LoadingOverlay';
import AsyncStorage from '@react-native-community/async-storage';
import Geolocation from "react-native-geolocation-service";

var do_items=[{id:1,do_name:'Play'},{id:2,do_name:'Celebrate'},{id:3,do_name:'Party'},{id:4,do_name:'Gather'},
                {id:5,do_name:'Meet'},{id:6,do_name:'Conference'},{id:7,do_name:'Celebrate'},{id:8,do_name:'Celebrate'}]

var drop_down_options=['Celebrate', 'Play','Party','Gather','Meet','Conference']
function GroupArray(data){
    var arrays = [], size = 4;
while (data.length > 0)
    arrays.push(data.splice(0, size));
return arrays;
}
export default class Home_Search extends Component{

        constructor(props){
            super(props)
           this.state = {
             scrollMove:0,
             loading:null,
             chosenlocation:null,searchloading: null,
             venuedata: props.venuedetails,
             doDropdown: {
               id: "venue_act_id",
               name: "venue_act_name",
               dropdown: []
             },
             activestate: true,
             doobj: null,
             whatDropdown: {
               id: "venue_purpose_id",
               name: "venue_purpose_name",
               dropdown: []
             },
             whatactiveobj: null,
             whereDropdown: {
               id: "venue_cat_id",
               name: "venue_cat_name",
               dropdown: []
             },
             whatobj: null,
             whereobj: null,
             activekey: 1,
             searchsliderdata: props.data ? props.data : [],
             arrowclicked: null,
             TOS: 0,
             loading: null
           };
            //    _dropdown_4_onSelect(idx,value)
            //    {
            //     this.state={value:props.valuedet}
            //    }         
          }
          sendDropdownData=(data,key,drop)=>{
            // alert('hii');
            this.setState({arrowclicked:null})
           this.active_items(data,key,drop);
          } 

          clearwhat=()=>{
            var whatDropdown=this.state.whatDropdown;
            whatDropdown.dropdown=[];
            this.setState({whatDropdown,whatobj:null});
          } 


     clearwhere=()=>{
            var whereDropdown=this.state.whereDropdown;
            whereDropdown.dropdown=[];
            this.setState({whereDropdown,whereobj:null});
          }

      loaddo=()=>{
        this.setState({loading:true})
    console.log("loaddo")
    fetch(links.APIURL+'do', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
        this.setState({loading:null})
        // this.setState({doDropdown:responseJson.data});
        var doDropdown=this.state.doDropdown;
          var getRows=[];
         if(responseJson.data.length>0){
           // alert();
           var getRows=[];
           var getdividerow=Math.floor(responseJson.data.length/8);
           var getremainrows=(responseJson.data.length%8);
           // alert(getremainrows);
           if(getdividerow>0){
             for(var i=0;i<getdividerow;i++){
               var allrows=responseJson.data.splice(0,8);
               getRows.push(allrows);
             }
           }
           if(getremainrows>0){
             var getremainrows=responseJson.data.splice(0,getremainrows);
             getRows.push(getremainrows);
           }

             // alert(JSON.stringify(getRows));
           // var getdivididrowby2=responseJson.data.splice
         }
    doDropdown.dropdown=getRows;
    // doDropdown.dropdown=responseJson.data;
    this.setState({doDropdown})

    })  
    this.setState({arrowclicked:null})
  }
  loadwhat(data){
    console.log("data",data);
        this.setState({loading:true})

    fetch(links.APIURL+'whatpurpose', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({'venue_act_id':data}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
        this.setState({loading:null})

    // alert(JSON.stringify(responseJson));
    if(responseJson.data.length==0){
        if(this.state.activekey!=null){
      // alert("no data found");
      Toast.show("No Records Found", Toast.LONG);
        this.setState({activekey:1});
        this.setState({doobj:null})
        // alert(JSON.stringify(this.state.doDropdown))
        // this.loaddo();
        return;
    }
    }
         var whatDropdown=this.state.whatDropdown;
           var getRows=[];
         if(responseJson.data.length>0){
           // alert();
           var getRows=[];
           var getdividerow=Math.floor(responseJson.data.length/8);
           var getremainrows=(responseJson.data.length%8);
           // alert(getremainrows);
           if(getdividerow>0){
             for(var i=0;i<getdividerow;i++){
               var allrows=responseJson.data.splice(0,8);
               getRows.push(allrows);
             }
           }
           if(getremainrows>0){
             var getremainrows=responseJson.data.splice(0,getremainrows);
             getRows.push(getremainrows);
           }

             // alert(JSON.stringify(getRows));
           // var getdivididrowby2=responseJson.data.splice
         }
    whatDropdown.dropdown=getRows;
    this.setState({whatDropdown})

   // console.log("what",responseJson);
       })  

    this.setState({arrowclicked:null})
  }
  loadwhere(data,data1){
        this.setState({loading:true})

    console.log("where_id",data);
    fetch(links.APIURL+'wherecategory', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({'venue_act_id':this.state.doobj.venue_act_id,'venue_purpose_id':data}),
    }).then((response)=>response.json())
    .then((responseJson)=>{
        this.setState({loading:null})

      // alert(JSON.stringify(responseJson));
        if(responseJson.data.length==0){
    if(this.state.activekey!=null){
          // alert("no data found");
      Toast.show("No Records Found", Toast.LONG);

        this.setState({activekey:1});
        this.setState({whatobj:null})
        this.setState({doobj:null})
        // this.loaddo();
        return;
    }
}
         var whereDropdown=this.state.whereDropdown;
           var getRows=[];
         if(responseJson.data.length>0){
           // alert();
           var getRows=[];
           var getdividerow=Math.floor(responseJson.data.length/8);
           var getremainrows=(responseJson.data.length%8);
           // alert(getremainrows);
           if(getdividerow>0){
             for(var i=0;i<getdividerow;i++){
               var allrows=responseJson.data.splice(0,8);
               getRows.push(allrows);
             }
           }
           if(getremainrows>0){
             var getremainrows=responseJson.data.splice(0,getremainrows);
             getRows.push(getremainrows);
           }

             // alert(JSON.stringify(getRows));
           // var getdivididrowby2=responseJson.data.splice
         }
    whereDropdown.dropdown=getRows;
    this.setState({whereDropdown})
    console.log("where",responseJson.data);

    })  

    this.setState({arrowclicked:null})
  } 

  async getaccess(){
  const granted= await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,{})
  // console.log('granted',granted);
  if(granted===PermissionsAndroid.RESULTS.GRANTED){
   return true;
  }else{
    return false;
  }
}
  async loadcurrentLocation(){
  const granted=await this.getaccess();
  if(granted==true){
  Geolocation.getCurrentPosition(
       (position) => {
         
         // alert(JSON.stringify(position));
         this.setState({chosenlocation:{lat:position.coords.latitude,lng:position.coords.longitude}});
       
      
       },
       (error) => this.setState({ error: error.message }),
       {timeout: 30000,maximumAge:0},
     );
}else{
  alert("Access Denied to get your location");
}
}
  arrowClick=async ()=>{  
     // alert('');
    const manuallocation = JSON.parse(await AsyncStorage.getItem('chosenlocation'));
    let lat = manuallocation!=null?manuallocation.lat:this.state.chosenlocation.lat;
    let lng = manuallocation!=null?manuallocation.lng:this.state.chosenlocation.lng; 
      
    this.setState({searchloading:true})
    fetch(links.APIURL + "dropdownSearchpurpose_new", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }, 
      body: JSON.stringify({
        venue_act_id: this.state.doobj ? this.state.doobj.venue_act_id : null,
        venue_purpose_id: this.state.whatobj
          ? this.state.whatobj.venue_purpose_id
          : null,
        venue_cat_id: this.state.whereobj
          ? this.state.whereobj.venue_cat_id
          : null,
        lat: lat,
        long:lng,
        offset:0,
        TOS: this.state.TOS
      })
    })
      .then(response => response.json())
      .then(responseJson => {
        
        this.setState({ arrowclicked: true, searchloading: null });
        // alert(JSON.stringify(responseJson))
        if (responseJson.status && responseJson.status == 1) {
          Toast.show("Options are empty to search", Toast.LONG);
        } else {
          // alert(responseJson.data.length)
          if (responseJson.data.length == 0) {
            Toast.show("No Records Found", Toast.LONG);
          } else {
            // this.setState({searchsliderdata:responseJson});
            this.props.nxtpge(responseJson.data, "", this.state,responseJson.count);
          }
        }
        // alert(JSON.stringify(responseJson));
        // Actions.Search_Home(JSON.stringify(responseJson));
        // alert(JSON.stringify(responseJson));
        // this.props.sendsliderdata(responseJson);
      });  
      //this.props.receivesearch(data);
      //this.setState({visible:true})
  }
  componentWillMount(){ 
   this.loadcurrentLocation();
    if (this.props.topcompobj) {
      this.state = this.props.topcompobj;
    }else{
 this.loaddo();
    }
   
  }
          active_items=(data,key,drop)=>{     
          // alert(JSON.stringify(data)) ;
               // this.setState({venuedata:item.id});
               this.setState({[key]:data});
               if(key=='doobj'){
                this.loadwhat(data.venue_act_id);
                this.setState({whatobj:null,TOS:1})
                if(drop){
                      this.clearwhat();
                      this.clearwhere();
                }else{

                this.setState({activekey:2});
                }
                }else if(key=='whatobj'){
                this.loadwhere(data.venue_purpose_id,this.state.doobj.venue_act_id);
                this.setState({whereobj:null,TOS:2})
                if(drop){
                  this.clearwhere();
                }else{
                this.setState({activekey:3});
                }
                }else{

                    this.setState({activekey:null,TOS:3})
                }
         
          //      this.setState({value:item.do_name})
           
 
              }

            //   _dropdown_4_onSelect(idx,value)
            //   {
            //    alert(`idx=${idx}, value='${value}'`);
            //   }

            renderSeparator = () => {  
                return (  
                    <View  
                        style={{  
                            height: 1,  
                            width: "25%",  
                            backgroundColor: "#000",  
                            flexDirection:'row',
                            justifyContent:'center'
                        }}  
                    />  
                );  
            };  
             


   componentWillReceiveProps(props){
     
          if(props.topcompobj){
           
                this.state=props.topcompobj
          } 
      }   
sendScrollingData=(event)=>{
  // alert(JSON.stringify());
  this.setState({scrollMove:event.nativeEvent.contentOffset.x})
}
updateScrollView=()=>{
   this.refs._scrollView.scrollTo({x:this.state.scrollMove,y:0,animated:true})
}
clearSearchDropdown=(key)=>{
  // alert(this.state.scrollMove);
if(key=='do'){
  this.setState({whatobj:null,whereobj:null,TOS:1},function(){
    this.setState({activekey:1},function(){
      var self=this;
      setTimeout(()=>{
     self.updateScrollView();
      })
    })

  })
}else if(key=='what'){
  this.setState({whereobj:null,TOS:2},function(){
    this.setState({activekey:2},function(){
     var self=this;
      setTimeout(()=>{
     self.updateScrollView();
      })
    })
  })
}else{
  this.setState({activekey:3,TOS:3},function(){
   var self=this;
      setTimeout(()=>{
     self.updateScrollView();
      })
  })
}
}
    render(){
        return(

          

<Content style={{backgroundColor:color.white}}>

            <View style={styles.container}> 
                    
                {!this.props.actionshide &&
                <View style={[styles.top_container,styles.conten_container]}>

                    <View style={{flexDirection:'row',flexWrap:'wrap',marginTop:hp('2%'),
                    alignItems:'center',
                                paddingLeft:wp('5%'),paddingRight:wp('5%'),  justifyContent:'flex-start',
                                  }}>
                        <View style={styles.view1}>
                            <Text style={styles.text_style1}>
                                I want to
                            </Text>
                        </View>
                        <View style={styles.view2}>
                        {this.state.doobj&&
                          <TouchableOpacity onPress={()=>this.clearSearchDropdown('do')}    style={[styles.tagContainer,styles.activeBG,styles.bgactivebg]}>
                        
         <Text numberOfLines={2} style={[styles.containerText,styles.acitveText]}>{this.state.doobj?this.state.doobj.venue_act_name:''}</Text>
         </TouchableOpacity>
       }
                        {/*}
                            <Dropdown_container
                                style={{height:hp('3.7%'),padding:3,width:'100%'}}
                                // _width={wp('55%')}
                                fakewidth={true}
                               
                                text_item={this.state.doobj?this.state.doobj.venue_act_name:''}
                                
                                values={this.state.doDropdown.dropdown}
                                 activestate={this.state.activekey}
                                 keyvalue={"venue_act_name"}
                                 value={this.state.doobj?this.state.doobj.venue_act_name:(!this.state.activekey?'Do':'')}
                                 
                                 sendDropdownData={(data)=>this.sendDropdownData(data,'doobj','drop')}
                                //  onSelect={(idx, value) => this._dropdown_4_onSelect(idx, value)}
                                    >
                            </Dropdown_container> */}   
                        </View>
                        {/*(this.state.whatobj || !this.state.activekey)&&
                             <View style={styles.view2}>
                            <Dropdown_container
                             fakewidth={true}
                                style={{height:hp('3.7%'),padding:3,width:'100%'}}
                                   text_item={this.state.whatobj?this.state.whatobj.venue_purpose_name:''}
                                values={this.state.whatDropdown.dropdown}

                                 activestate={this.state.activekey}
                                 keyvalue={"venue_purpose_name"}
                                 value={this.state.whatobj?this.state.whatobj.venue_purpose_name:(!this.state.activekey?'What':'')}
                                 sendDropdownData={(data)=>this.sendDropdownData(data,'whatobj','drop')}
                                //  onSelect={(idx, value) => this._dropdown_4_onSelect(idx, value)}
                                    >
                            </Dropdown_container>    
                        </View>
                    */}
                    <View style={styles.view2}>
                    {this.state.whatobj&&
                       <TouchableOpacity onPress={()=>this.clearSearchDropdown('what')}    style={[styles.tagContainer,styles.activeBG,styles.bgactivebg]}>
                        
         <Text numberOfLines={2} style={[styles.containerText,styles.acitveText]}>{this.state.whatobj?this.state.whatobj.venue_purpose_name:''}</Text>
         </TouchableOpacity>
                    }
                    </View>

                     {/*(this.state.whatobj || !this.state.activekey)&&
                             <View style={[styles.view2,{flexDirection:'row'}]}>
                             <Text style={{color:color.white,fontSize:hp('2%')}}> in </Text>
                            <Dropdown_container
                             fakewidth={true}
                                style={{height:hp('3.7%'),padding:3,width:'100%'}}
                                   text_item={this.state.whereobj?this.state.whereobj.venue_cat_name:''}
                                values={this.state.whereDropdown.dropdown}
                                 activestate={this.state.activekey}
                                  keyvalue={"venue_cat_name"}
                                 value={this.state.whereobj?this.state.whereobj.venue_cat_name:(!this.state.activekey?'Where':'')}
                                 sendDropdownData={(data)=>this.sendDropdownData(data,'whereobj','drop')}
                                //  onSelect={(idx, value) => this._dropdown_4_onSelect(idx, value)}
                                    >
                            </Dropdown_container>    
                        </View>
                    */}
                     <View style={[styles.view2,{marginLeft:0}]}>
                    {this.state.whereobj&&
                      <View style={{flexDirection:'row',alignItems:'center'}}>
                      <Text style={{fontSize:hp('2%'),marginRight:5,marginBottom:2,color:color.dullblue,fontWeight:'bold'}}>in</Text>
                       <TouchableOpacity onPress={()=>this.clearSearchDropdown('where')}   style={[styles.tagContainer,styles.activeBG,styles.bgactivebg]}>
                        
         <Text numberOfLines={2} style={[styles.containerText,styles.acitveText]}> {this.state.whereobj?this.state.whereobj.venue_cat_name:''}</Text>
         </TouchableOpacity>
         </View>
                    }
                    </View>
                        {/* <View style={styles.view2}>
                            <Dropdown_container style={{height:hp('3.7%'),padding:3}}
                            text_item={'Soccer'}>
                            </Dropdown_container> 
                        </View>
                        <View style={styles.view1}>
                            <Text style={styles.text_style1}>
                                in
                            </Text>    
                        </View>
                        <View style={styles.view2}>
                            <Dropdown_container style={{height:hp('3.7%'),padding:3}}
                            text_item={'Soccer Club Academy'}>
                                
                            </Dropdown_container> 
                        </View> */}
                        
                        <View style={styles.view3}>
{this.state.searchloading==true&&this.state.doobj&&
 <TouchableOpacity style={{backgroundColor:color.dullblue,borderRadius:14,height:hp('4%'),paddingLeft:wp('3%'),paddingRight:wp('3%')}}>
                                <View  style={{alignSelf:"center",paddingTop:5,paddingBottom:5}}>

    <ActivityIndicator size="small" color={color.white} style={{paddingTop:0,paddingBottom:2}} />
                                    
                                </View>
                            </TouchableOpacity>
}
              {!this.state.searchloading==true&&this.state.doobj&&
                            <TouchableOpacity  onPress={()=>this.arrowClick()} style={{backgroundColor:color.dullblue,borderRadius:14,height:hp('4%'),paddingLeft:wp('4%'),paddingRight:wp('4%')}}>
                                <View  style={{alignSelf:"center",paddingTop:8,paddingBottom:5}}>

                                    <Image style={{width:hp('2%'),height:hp('2%'),alignSelf:"center"}} 
                                        source={Right_arrow}>
                                </Image>
                                </View>
                            </TouchableOpacity>
                          }
                        </View>
                    </View>
       {/* do dropdown*/}
       <View style={{flexDirection:"row",alignItems:'center',justifyContent:'center'}}>
       {this.state.loading&&
          <ActivityIndicator size="large" color={color.blueactive} style={{paddingTop:0,paddingBottom:2}} />
       }
       </View>
       {this.state.activekey==1&&!this.state.loading&&
           <ScrollView  ref='_scrollView'  showsHorizontalScrollIndicator={false}  onScroll={event =>this.sendScrollingData(event)}
  scrollEventThrottle={16} horizontal={true} >
          
               {this.state.doDropdown.dropdown.map((objdata1,indexkey)=>{
                   return(
               <View  style={{flexDirection:'row',flexWrap:'wrap',width:wp('100%'),alignItems:'flex-start',justifyContent:'space-between'}}>

                     {objdata1.map((objdata)=>{
                       return(
                         <View style={styles.list_style}>
                            <View>

                            <TouchableOpacity onPress={()=>this.active_items(objdata,'doobj')}   key={indexkey} style={[styles.tagContainer,this.state.doobj?(objdata.venue_act_id==this.state.doobj.venue_act_id?styles.newactiveBG:null):null]}>
         <Text numberOfLines={2} style={[styles.containerText,this.state.doobj?(objdata.venue_act_id==this.state.doobj.venue_act_id?styles.acitveText:null):null]}>{objdata.venue_act_name}</Text>
         </TouchableOpacity>
                            {/*
                            <CircleComp
                             innerText={true}
                                sendText={(data)=>this.active_items(objdata,'doobj')}                            
                                text={objdata.venue_act_name} 
                                
                                 width={12} height={12} >
                            </CircleComp>*/}
                            </View>
                        </View>
                        )
                        })}
                      <View style={[styles.list_style,{marginTop:0}]}>
                    <TouchableOpacity style={[styles.tagContainer,{borderWidth:0,height:0}]}/>
                     </View>
                      <View style={[styles.list_style,{marginTop:0}]}>
                    <TouchableOpacity style={[styles.tagContainer,{borderWidth:0,height:0}]}/>
                     </View>     
                        </View>
                       )
               })}
             
           </ScrollView>

                     }
        {/* what dropdown*/}
        {this.state.activekey==2&&!this.state.loading&&
              <ScrollView  ref='_scrollView' scrollEventThrottle={16} onScroll={event =>this.sendScrollingData(event)} showsHorizontalScrollIndicator={false} horizontal={true}>
           
               {this.state.whatDropdown.dropdown.map((objdata1,indexkey)=>{
                 return (
               <View style={{flexDirection:'row',flexWrap:'wrap',width:wp('100%'),alignItems:'flex-start',justifyContent:'space-between'}}>
               {objdata1.map((objdata)=>{
                   return(
                         <View style={styles.list_style}>
                            <View>
                            {/*}
                            <CircleComp
                            innerText={true}
                                sendText={(data)=>this.active_items(objdata,'whatobj')}                            
                                text={objdata.venue_purpose_name} 
                                
                                 width={12} height={12} >
                            </CircleComp>*/}
                               <TouchableOpacity onPress={()=>this.active_items(objdata,'whatobj')}  key={indexkey} style={[styles.tagContainer,this.state.whatobj?(objdata.venue_purpose_id==this.state.whatobj.venue_purpose_id?styles.newactiveBG:null):null]}>
         <Text numberOfLines={2} style={[styles.containerText,this.state.whatobj?(objdata.venue_purpose_id==this.state.whatobj.venue_purpose_id?styles.acitveText:null):null]}>{objdata.venue_purpose_name}</Text>
         </TouchableOpacity>
                            </View>
                        </View>
                       )
                 })}
                 <View style={[styles.list_style,{marginTop:0}]}>
                    <TouchableOpacity style={[styles.tagContainer,{borderWidth:0,height:0}]}/>
                     </View>
                      <View style={[styles.list_style,{marginTop:0}]}>
                    <TouchableOpacity style={[styles.tagContainer,{borderWidth:0,height:0}]}/>
                     </View>    
                   </View>
                   )
               })}
               </ScrollView>                   
                     }
                 {/* where dropdown*/}
                     {this.state.activekey==3&&!this.state.loading&&
                                  <ScrollView  ref='_scrollView' onScroll={event =>this.sendScrollingData(event)} scrollEventThrottle={16} showsHorizontalScrollIndicator={false} horizontal={true}>
              
               {this.state.whereDropdown.dropdown.map((objdata1,indexkey)=>{
                   return(
               <View style={{flexDirection:'row',flexWrap:'wrap',width:wp('100%'),alignItems:'flex-start',justifyContent:'space-between'}}>
               {objdata1.map((objdata,keydata)=>{
                        return(
                         <View style={styles.list_style}>
                            <View>
                            {/*<CircleComp
                            innerText={true}
                                sendText={(data)=>this.active_items(objdata,'whereobj')}                            
                                text={objdata.venue_cat_name} 
                                
                                 width={12} height={12} >
                            </CircleComp>*/}
                             <TouchableOpacity onPress={()=>this.active_items(objdata,'whereobj')}  key={indexkey} style={[styles.tagContainer,this.state.whereobj?(objdata.venue_cat_id==this.state.whereobj.venue_cat_id?styles.newactiveBG:null):null]}>
         <Text numberOfLines={2} style={[styles.containerText,this.state.whereobj?(objdata.venue_cat_id==this.state.whereobj.venue_cat_id?styles.acitveText:null):null]}>{objdata.venue_cat_name}</Text>
         </TouchableOpacity>
                            </View>
                        </View>
                       )
                      
               })}
                    <View style={[styles.list_style,{marginTop:0}]}>
                    <TouchableOpacity style={[styles.tagContainer,{borderWidth:0,height:0}]}/>
                     </View>
                      <View style={[styles.list_style,{marginTop:0}]}>
                    <TouchableOpacity style={[styles.tagContainer,{borderWidth:0,height:0}]}/>
                     </View>  
               </View>
                      )
                 })}
              </ScrollView>
                     }
                </View>
              }
                
                
           </View>          
                 </Content>
        )
    }
} 

const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:color.white
    },
    top_container:{
        backgroundColor:color.white,
        // minHeight:hp('30%'),
        justifyContent:'center',
        alignItems:'center',
           width:'100%',
           // padding:10
    },
    list_style:{
     //   flex:1,
     marginLeft:wp('1.25%'),
     marginRight:wp('1.25%'),
        marginTop:hp('2.5%'),
        justifyContent:'flex-start',
        alignItems:'flex-start',
        alignSelf:'flex-start',
        alignContent:'flex-start',
    },
    circleStyle:{
        width:wp('20%'),
    },
    conten_container:{
   //     flex:1,
     //   flexDirection:'row',
       // justifyContent:'space-between',
       // flexWrap:'wrap',
        justifyContent:'space-between',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'space-between',
        paddingBottom:15
    },
    text_style1:{
        color:color.dullblue,
        fontWeight:'bold',
        fontSize:hp('3.2')
    },
    view1:{
     //   flex:.3,
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center',
        paddingRight:hp('1%'),
        paddingBottom:hp('2%'),
        height:hp('6%'),
        // backgroundColor:'grey'
    },
    view2:{
       // flex:.5,
    //   width:'100%',
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center',
        marginLeft:10,
        marginRight:5,
        paddingBottom:10,
    },
    view3:{
       // flex:.2,
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center',
        paddingBottom:12,
    },
    blue_btn:{
        width:wp('10%'),
        justifyContent:'center',
        padding:3,
        backgroundColor:color.blue,
        borderRadius:15
    },
  tagContainer:{

    borderColor:color.orange,marginRight:0,alignSelf: 'flex-start',borderWidth:1,fontSize:hp('2%'),width:'auto',marginBottom:6,borderColor:color.dullblue,borderRadius:50,width:wp('22%'),height:hp('5%'),alignItems:'center',padding:0,flexDirection:'row',justifyContent:'center',paddingHorizontal:5,
  },
  
  containerText:{textAlign:'center',fontSize:wp('3.1%'),color:color.blueactive},
    activeBG:{backgroundColor:color.dullorange,borderColor:color.dullorange,width:'auto',height:'auto',minWidth:hp('12%'),padding:5,paddingHorizontal:12,height:hp('5.1%')},
    newactiveBG:{backgroundColor:color.dullorange,borderColor:color.dullorange},
  acitveText:{color:color.white,textAlign:'center',fontWeight:'bold'},

})
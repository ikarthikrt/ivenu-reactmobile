import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Alert, StatusBar, Modal, Image, TouchableHighlight, TouchableOpacity, ImageBackground,ScrollView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Container, Icon, Button, Subtitle, Header, Content, Body, Right, Title, Item, Input,ListItem,Left } from 'native-base';
import color from '../Helpers/color';
import { Actions, Router, Scene } from 'react-native-router-flux';
import ModalComp from '../components/ModalComp';
var sharemoreabout=[{name:"Any Local Trainers Venues near you ?",question_id:1,ans_status:null},
                    {name:"Venue needs in your location ?",question_id:2,ans_status:null},
                    {name:"Is your neighborhood undeserved ?",question_id:3,ans_status:null} ,
                    {name:"Venue needs in your location ?",question_id:4,ans_status:null},
                    {name:"Any Local Training Venues near you ?",question_id:5,ans_status:null}];
import yes from '../images/svg/Yes_Symbol1.png';
import no from '../images/svg/No_Symbol1.png';
export default class ShareMore extends React.Component {
	constructor(props){
		super(props);
    this.state={
      sharemoreDetails:sharemoreabout,
      profileData:props.signupdata
    }

	}
	skip=()=>{
		if(this.props.modalclose){
			this.props.modalclose();
      var signupdata=this.state.profileData;
    signupdata.share_details=[];
    this.props.submitData({signupdata});
		}
	}
  changeColor=(state,value,index)=>{
    var sharemoreDetails=this.state.sharemoreDetails;


    sharemoreDetails[index][state]=value;

      this.setState({sharemoreDetails});
  }
  submitsharemore=()=>{
    var signupdata=this.state.profileData;
    signupdata.share_details=this.state.sharemoreDetails;
    this.props.submitData({signupdata});
    // alert(JSON.stringify(this.state.sharemoreDetails));

  }
renderList=(data,key)=>{
	return(
		<View key={key} style={styles.mainlistview}>
            <View style={{flex:0.8}}>
              <Text style={styles.sharetext}>{data.name}</Text>
            </View>
            <View style={{flex:0.2,flexDirection:'row',justifyContent:'flex-end',paddingRight:12}}>
            <View style={{marginRight:12}}>
            <TouchableOpacity onPress={()=>this.changeColor('ans_status',true,key)}>
               <View style={{borderColor:color.orange,borderWidth:1,width:hp('4.5%'),height:hp('4.5%'),borderRadius:hp('4.5%')/2,backgroundColor:data.ans_status==true?color.orange:color.white,alignItems:'center',justifyContent:'center'}}>
<Image tintColor={data.ans_status==true?color.white:color.orange} source={yes} style={{width:'90%',height:'90%'}}/>
               </View>
                </TouchableOpacity>
                </View>
                <View>
               <TouchableOpacity onPress={()=>this.changeColor('ans_status',false,key)}>
                <View style={{borderColor:color.orange,borderWidth:1,width:hp('4.5%'),height:hp('4.5%'),borderRadius:hp('4.5%')/2,backgroundColor:data.ans_status==false?color.orange:color.white,alignItems:'center',justifyContent:'center'}}>
<Image tintColor={data.ans_status==false?color.white:color.orange} source={no} style={{width:'90%',height:'90%'}}/>
               </View>
                </TouchableOpacity>
                </View>
            </View>
            </View>
            )
}
	render(){
		return(
			<Content>
			<View style={{borderBottomWidth:0.7,borderColor:color.black1,padding:12}}>
		<Body style={{alignItems:'center',justifyContent:'center'}}>
            <Title style={{color:color.black,fontSize:hp('3%')}}>Hello {this.state.profileData.name},</Title>
				<View style={{marginTop:5}}></View>
            <Title style={{color:color.orange,fontSize:hp('4%')}}>Share more about you</Title>
            <Title style={{color:color.black1,fontSize:hp('2.3%')}}>and allow us to serve you better!</Title>
            </Body>
            </View>
             {this.state.sharemoreDetails.map((obj,key)=>{
               return(
               this.renderList(obj,key)
               )
             })}
            
             <View style={styles.submitbox}>
     <View style={{flex:0.5,flexDirection:'row',justifyContent:'flex-start'}}>
       <Button onPress={()=>this.skip()} style={styles.actionbtn}>
            <Text style={styles.actionbtntxt}>SKIP NOW</Text>
          </Button>
          </View>
          <View style={{flex:0.5,flexDirection:'row',justifyContent:'flex-end'}}>
          <Button onPress={()=>this.submitsharemore()} style={[styles.actionbtn,{backgroundColor:color.orange}]}>
           <Text style={styles.actionbtntxt} >NEXT</Text>
          </Button>
          </View>
     </View>
			</Content>
			)
	}

}
const styles={
	actionscoloricon:{fontSize:hp('4%'),color:color.black1},
	sharetext:{fontSize:hp('2.2%')},
	mainlistview:{flex:1,flexDirection:'row',alignItems:'center',   paddingLeft:12,
    paddingRight:12,
    paddingTop:17,
    paddingBottom:17,borderBottomWidth:0.5,borderColor:color.black1},
	submitbox:{
		flex:1,
		flexDirection:'row',
		padding:12,
		borderBottomWidth:0.5
	},actionbtn:{
		borderRadius:5,
		width:hp('20%'),
		justifyContent:'center',
	},
	actionbtntxt:{
		textAlign:'center',
		color:color.white,
		fontSize:hp('2.3%')
	},
}
import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator
} from "react-native";
import color from "../Helpers/color";

import AsyncStorage from "@react-native-community/async-storage";
import {
  Container,
  Header,
  Content,
  Tab,
  Tabs,
  List,
  ListItem,
  Item,
  Input,
  Thumbnail,
  Left,
  Body,
  Right,
  Textarea,
  Label,
  Button,
  ScrollableTab,
  Icon,
  Form
} from "native-base";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import moment from "moment";
import links from "../Helpers/config";
import DateFunctions from "../Helpers/DateFunctions";
import ChatView from "./chatView";
import Toast from "react-native-simple-toast";

const default_avator = "../images/avatar.png"; 

export default class SingleChat extends Component {
                 constructor(props) {
                   super(props);
                   this.state = {
                     activeVenue: props.data,
                     showloading: false,
                     clear: true,
                     messages: []
                   }; 

                   console.log("props", props.data);
                 }

                 componentWillMount = () => {
                   this.checkingLogin();
                 };

                 componentWillUnmount() {
                   console.log("willunmount");
                   clearInterval(this.timer);
                 }

                 refresh = () => {
                   console.log("timer called");
                   if (!this.state.clear) this.getVenueMessage();
                 };

                 async checkingLogin() {
                   const data = await AsyncStorage.getItem("loginDetails");

                   if (data != null) {
                     var parsedata = JSON.parse(data);
                     this.setState(
                       {
                         loginDetails: parsedata,
                         showloading: true
                       },
                       function() {
                         this.getVenueMessage();
                       }
                     );
                   } else this.getVenueMessage();
                 }

                 componentDidMount = () => {
                   console.log("fired_5s");
                   this.timer = setInterval(() => {
                    this.getVenueMessage()
                   }, 5000);
                 };

                 getVenueMessage = item => {
                   const { activeVenue } = this.state;
                   console.log({
                     myUserId: this.state.loginDetails
                       ? this.state.loginDetails.user_id
                       : "0",
                     customerUserId: activeVenue.venue_user_id,
                     venueId: activeVenue.venue_id
                   });

                   fetch(links.APIURL + "getChatMessages", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify({
                       myUserId: this.state.loginDetails
                         ? this.state.loginDetails.user_id
                         : "0",
                       customerUserId: activeVenue.venue_user_id,
                       venueId: activeVenue.venue_id
                     })
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       console.log("respfgsd", responseJson);

                       if (responseJson.status != 0) {
                         //  Toast.show("No Records Found", Toast.LONG);
                         this.setState({ showloading: false });
                       } else if (
                         responseJson.status == 0 &&
                         responseJson.data.length > 0
                       ) {
                         var data =
                           responseJson.data.length > 0
                             ? responseJson.data
                             : [];
                         this.setState({
                           showloading: false,
                           messages: data
                         });
                       }
                     });
                 };

                 pushMessage = item => {
                   var data = item;
                   data.chatSenderId = this.state.loginDetails
                     ? this.state.loginDetails.user_id
                     : "0";

                   console.log("sendmesg", data);

                   fetch(links.APIURL + "sendMessage", {
                     method: "POST",
                     headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json"
                     },
                     body: JSON.stringify(data)
                   })
                     .then(response => response.json())
                     .then(responseJson => {
                       console.log("mesgresposn", responseJson);
                       if (responseJson.status == 0) {
                         this.getVenueMessage();
                       }
                     });
                 };

                 render() {
                   const { messages, clear, activeVenue } = this.state;
                   return (
                     <View
                       style={{
                         flex: 1,
                         justifyContent: "center",
                         alignItems: "center"
                       }}
                     >
                       <View
                         style={{
                           flex: 1,
                           width: wp("100%"),
                           height: hp("100%")
                         }}
                       >
                         <List
                           style={{ backgroundColor: "white" }}
                           noBorder
                           noIndent
                         >
                           <ListItem avatar noBorder noIndent>
                             <Left>
                               <Thumbnail
                                 source={{
                                   uri:
                                     this.state.activeVenue.venue_image_path !=
                                     null
                                       ? this.state.activeVenue.venue_image_path
                                       : default_avator
                                 }}
                               />
                             </Left>
                             <Body>
                               <Text>
                                 {this.state.activeVenue.trn_venue_name}
                               </Text>
                               <Text numberOfLines={1} note>
                                 {this.state.activeVenue.trn_venue_address}
                               </Text>
                             </Body>
                           </ListItem>
                         </List>

                         <ChatView
                           messages={messages}
                           userId={activeVenue}
                           refresh={this.refresh}
                           clear={this.state.clear}
                           pushMessage={this.pushMessage}
                          
                         />
                       </View>
                     </View>
                   );
                 }
               }

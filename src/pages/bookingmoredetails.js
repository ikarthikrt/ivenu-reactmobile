'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
     RefreshControl,
  ScrollView,
  WebView
} from 'react-native';
import { Actions } from "react-native-router-flux";
import DateFunctions from '../Helpers/DateFunctions';
import color from '../Helpers/color';
import { Container, Header, Content, Tab, Tabs,Accordion,Icon } from 'native-base';
import dashboardimage from '../images/bookings.png';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import BookingCard from '../components/BookingCard'
import links from '../Helpers/config';
import AsyncStorage from '@react-native-community/async-storage';
import moment from "moment";
import Line from '../components/Line'
import ModalComp from '../components/Modal';
class BookingMoreDetails extends Component {
  constructor(props) {
    super(props);
  
    this.state = {bookingDetails:null,refreshing:false,collapse:false,cancellation:false};
  }
  generateDays=(type,count)=>{
    if(type==1){
      return "Hours";
    }else{
      if(type=='4'){
  return ("(Monthly)"+" - "+(Math.ceil(count/30)>1?Math.ceil(count/30)+' Months':Math.ceil(count/30)+' Month'));
  
}else if(type=='3'){
  return ("(Weekly)"+" - "+(Math.ceil(count/7)>1?Math.ceil(count/7)+' Weeks':Math.ceil(count/7)+' Week'));
  
}else{
return ("(Daily)"+" - "+(count>1?count+' Days':count+' Day'));

}
    }
  }
  openCancellationPolicy=()=>{
    // alert('cancel')
    this.setState({cancellation:true})
  }
  cancelBooking=()=>{
    alert('cancel')
  }
  render() {
    const bookdetails=this.props.bookdetails;
    const PolicyHTML = require('./cancellationpolicy.html');
    // const {bookingDetails} =this.props;
      var dateonbooked=(bookdetails.trn_booked_date)?moment(DateFunctions.getMobileDate(bookdetails.trn_booked_date)).format('DD MMM / YYYY'):"-";
      var venuetype=bookdetails.trn_venue_type==2?'Pax':(bookdetails.trn_venue_type==3?'Seat':'Venue');
    return (
       <Container>
       <ScrollView>
          {bookdetails&&
              <BookingCard goback={()=>Actions.pop()} backarrow={true} subtitle={venuetype} leftHeader={bookdetails.trn_venue_name.toUpperCase()} rightHeader={dateonbooked}>
              <>
             <View style={styles.bodycard}>
             <View>
             {(bookdetails.trn_venue_type==2||bookdetails.trn_venue_type==3)&&
             <Text style={{color:color.blueactive,fontWeight:'bold',fontSize:hp('2%')}} >{(bookdetails.trn_venue_type==2?bookdetails.pax_name +` - (${bookdetails.pax_qty} Paxes)`:bookdetails.seat_name+` - (${bookdetails.seat_qty} Seats)`).toUpperCase()} </Text>
             }
             <Text style={{color:color.blueactive,fontSize:hp('2%')}} >BOOKING ID : {bookdetails.booking_uniqId}</Text>
             </View>
             <Text style={{color:color.top_red,fontSize:hp('2%')}}>{this.props.status}</Text>

             </View>
             <Line margin={12}/>
             {bookdetails.trn_venue_type==3&&bookdetails.trn_availability_type!=1&&
               <View style={{flexDirection:'row',justifyContent:'space-between'}}>
               <View>
               <Text style={styles.bodytext}>{moment(DateFunctions.getMobileDate(bookdetails.trn_booking_from_date_time)).format('DD MMM / YYYY') +" - "+ moment(DateFunctions.getMobileDate(bookdetails.trn_booking_to_date_time)).format('DD MMM / YYYY')}</Text>
               <Text style={styles.bodytext}>{this.generateDays(bookdetails.trn_availability_type,Array.isArray(bookdetails.bookedTimingDetails)==true&&bookdetails.bookedTimingDetails.length>0&&bookdetails.bookedTimingDetails[0].dateCount)}</Text>
               </View>
               <Text style={styles.bodytext}> {bookdetails.rate} <Text style={[styles.bodytext,{fontWeight:'bold'}]}>{bookdetails.currency}</Text></Text>
               </View>
             }
             {bookdetails.trn_venue_type==2&&
               <View style={{flexDirection:'column'}}>
               <View>
               <View style={{flexDirection:'row',justifyContent:'space-between'}}>
               <Text>{moment(DateFunctions.getMobileDate(bookdetails.trn_booking_from_date_time)).format('DD MMM / YYYY')}</Text>
                <Text style={styles.bodytext}> {bookdetails.rate} <Text style={[styles.bodytext,{fontWeight:'bold'}]}>{bookdetails.currency}</Text></Text>
               </View>
               <Text>{bookdetails.pax_rate_type}</Text>
               </View>
               <Line margin={5}/>
                 {bookdetails.bookedTimingDetails.map((obj)=>{
                   return(
                     <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                     <Text style={styles.bodytext}>{obj.pax_rate} x{obj.pax_qty} </Text>
                     <Text style={styles.bodytext}>{obj.pax_cost} <Text style={[styles.bodytext,{fontWeight:'bold'}]}>{bookdetails.currency} </Text></Text>
                     </View>
                     )
                 })}
               </View>
             }
             {(bookdetails.trn_venue_type!=2)&&bookdetails.trn_availability_type==1&&
               <>
               <View style={{flexDirection:'row',justifyContent:'space-between',marginVertical:5}}>
               <View>
              
               <Text style={styles.bodytext}>{bookdetails.bookedTimingDetails.reduce((acc,objdata)=>objdata.slotDetails.length+acc,0)} Slots</Text>
               </View>
               <Text style={styles.bodytext}> {bookdetails.rate} <Text style={[styles.bodytext,{fontWeight:'bold'}]}>{bookdetails.currency}</Text></Text>
               </View>
             
               {this.state.collapse==true&&bookdetails.bookedTimingDetails.map((objnew)=>{
                 return(
                     <View>
                       <Text style={[styles.bodytext,{fontWeight:'bold'}]}>{moment(objnew.date).format('DD MMM / YYYY ,')} <Text >{objnew.slotDetails.length} SLOTS</Text> </Text>
                       <View style={{flexDirection:'row',flexWrap:'wrap',marginVertical:5}}>
                       {objnew.slotDetails.map((objtime)=>{
                         return(
                             <Text style={[styles.bodytext]}>{DateFunctions.converttime24to12(objtime.booking_slot_fromtime)+" - "+DateFunctions.converttime24to12(objtime.booking_slot_totime)+" | "}</Text>
                           )
                       })}
                      </View>
                       
                     </View>
                   )
               })}
                 <TouchableOpacity style={{flexDirection:'row',alignItems:'center'}} onPress={()=>this.setState({collapse:!this.state.collapse})}>{this.state.collapse==false?<View style={{flexDirection:'row',flex:1,alignItems:'center'}}><Text style={{textAlign:'center',marginRight:5}}>Show More{' '}</Text><Icon type='FontAwesome' name="angle-down" style={{color:color.black1}}/></View>:<View style={{flexDirection:'row',flex:1,alignItems:'center'}}><Text style={{textAlign:'center',marginRight:5}}>Show Less{' '}</Text><Icon type='FontAwesome' name="angle-up" style={{color:color.black1}}/></View>}
                 </TouchableOpacity>

               </>
             }
             <Line margin={12}/>
             <View style={{flex:1,flexDirection:'row',justifyContent:'space-between'}}>
             <View>
             <Text style={styles.bodytext}>{bookdetails.trn_booking_name.toUpperCase()}</Text>
             <Text style={styles.bodytext}>{bookdetails.trn_booking_email}</Text>
             <Text style={styles.bodytext}>{bookdetails.trn_booking_phone}</Text>
             </View>
             {/*<View>
               <TouchableOpacity onPress={()=>this.openCancellationPolicy()}>
               <Text>Cancellation Policy</Text>
               </TouchableOpacity>
               <TouchableOpacity onPress={()=>this.cancelBooking()}>
               <Text>Cancel</Text>
               </TouchableOpacity>
             </View>*/}
             </View>
             </>
             </BookingCard>
          }
          </ScrollView>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
bodycard:{
  flex:1,
  flexDirection:'row',
  justifyContent:'space-between',
  alignItems:'center',
},
bodytext:{
  fontSize:hp('2%')
}
});


export default BookingMoreDetails;
import React,{Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,Image,Alert,FlatList} from 'react-native';
import {Right,Top,Icon, Button,ListItem,Content} from 'native-base'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import fb_png from '../images/fb.png';
import insta_png from '../images/insta.png';
import google_png from '../images/google.png'
import ModalComp from '../components/Modal';
import AsyncStorage from '@react-native-community/async-storage';
import dateFormat from 'dateformat';
export default class SuccessPage extends Component{
	constructor(props) {
		// alert(JSON.stringify(props));
	  super(props);
	  this.state = {loginDetails:null};
	}
// switchcorporate=()=>{
// Alert.alert(
//   'Alert',
//   'Switch To Corporate Login?',
//   [
//     {
//       text: 'Cancel',
//       onPress: () => console.log('cancelled'),
//       style: 'cancel',
//     },
//     {text: 'OK', onPress: () =>{

//         this.props.closemodal('corplogin')

//     } },
//   ],
//   {cancelable: false},
// );
// }
componentDidMount(){
     AsyncStorage.getItem('loginDetails', (err, result) => {
      
       if(result!=null){
    this.setState({loginDetails:JSON.parse(result)})
      }else{
    this.setState({loginDetails:null})

      }
     });
  }
    render(){
        // alert(JSON.stringify(this.props.loginDetails))
        return(
            
            <Content style={{height:hp('85%')}}>
            <View style={{height:'100%',paddingTop:hp('15%'),padding:10}}>
                <View style={{flex:.1}}></View>
                <View style={styles.view1}>
                    <View style={styles.view2}>
                        <Text style={styles.txt1}>Hello <Text style={{fontWeight:'bold'}}>{this.state.loginDetails&&this.state.loginDetails.user_name}</Text></Text>
                        <Text style={styles.txt2}></Text>
                    </View>
                    <View style={styles.view3}>
                        <Text style={styles.txt3}>You have booked the Venue</Text>
                        <Text style={styles.txt4}>Successfully!</Text>
                    </View>
                    
                    <View style={styles.view5}>
                        <Text style={styles.txt6}>{this.props.roomname}</Text>
                        <Text style={styles.txt5}>Booking Details</Text>
                    </View>
                    <View style={styles.view5}>
                        <Text style={styles.txt6}>{this.props.booksuccess.trn_venue_name}</Text>
                        <Text style={styles.txt5}>{this.props.booksuccess.bookdetails.venudate}</Text>
                    </View>
                    
                </View>    
                <View style={{flex:.1,marginTop:hp('3%'),marginBottom:hp('4%'),flexDirection:'row'}}>
                <View style={{flex:0.5,justifyContent:'flex-end'}}><Text style={{textAlign:'right'}}>Share with</Text>
                </View>
                    <View style={styles.view6}>
                        <TouchableOpacity><Image style={styles.socialicon} source={google_png}/></TouchableOpacity>
	                    <TouchableOpacity><Image style={styles.socialicon} source={fb_png}/></TouchableOpacity>
	                    <TouchableOpacity><Image style={styles.socialicon} source={insta_png}/></TouchableOpacity>
                    </View>
                </View>
                
                <View style={{flex:.1,flexDirection:'row'}}>
                    <View style={styles.view12}>
                        <Button style={styles.cancel_btn} onPress={()=>this.props.closemodal()}>
                            <Text style={styles.cancel_txt}> BOOK MORE </Text>
                        </Button>
                    </View>
                    <View style={styles.view13}>
                        <Button style={styles.cancel_btn} onPress={()=>this.props.closemodal()}>
                            <Text style={styles.cancel_txt}> ADD VENUE </Text>
                        </Button>
                    </View>
                </View>    
            </View>
            </Content>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        flex:1,
        height:hp('80%'),
        backgroundColor:color.white, 
        paddingLeft:hp('2%'),
        paddingRight:hp('2%'),
    },
    view1:{
        flex:.5,
        flexDirection:'column',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center'
    },
    view2:{
        flexDirection:'row'
    },
    view3:{
        marginTop:hp('2%'),
        marginBottom:hp('4%')
    },
    view4:{
        marginBottom:hp('3%')
    },
    view5:{
        marginTop:hp('2%'),  
        marginBottom:hp('3%')
    },
    view6:{
    	flex:0.5,
        flexDirection:'row',
        justifyContent:'flex-start',
        paddingLeft:12,
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center'
    },



    txt1:{
        marginRight:3,
        fontSize:hp('2%')
    },
    txt2:{
        fontWeight:'bold',
        fontSize:hp('2%')
    },
    txt3:{
        color:color.orange,
        fontSize:hp('2.9%')
    },
    txt4:{
        color:color.orange,
        fontSize:hp('3.3%'),
        flexDirection:'row',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center',
        fontWeight:'bold'
    },
    txt5:{
        fontSize:hp('2.5%'),
        flexDirection:'row',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center'
    },
    txt6:{
        fontSize:hp('3.3%'),
        flexDirection:'row',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center'
    },
    txt7:{
        color:color.black,
        fontSize:hp('2%'),
        flexDirection:'row',
        justifyContent:'center',
        alignSelf:'center',
        alignContent:'center',
        alignItems:'center',
        marginRight:8,
        
    },
    socialicon:{
		width:hp('4%'),
		height:hp('4%'),
        margin:2
    },
    view12:{
        flex:1,
        justifyContent:'flex-end',
    },
    view13:{
        flex:1,
        justifyContent:'flex-end',
        alignSelf:'flex-end',alignContent:'flex-end',alignItems:'flex-end',
        flexDirection:'row'
    },
    cancel_txt:{
        fontSize:hp('2%'),
        color:color.white
    },
    cancel_btn:{
        paddingLeft:10,
        paddingRight:10,
        backgroundColor:color.orange,
        borderRadius:5,
        padding:10,
        height:hp('5%'),
    },
})
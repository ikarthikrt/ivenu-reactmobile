import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Alert,StatusBar,Modal,Image,TouchableHighlight,ToastAndroid,TouchableOpacity,ImageBackground} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Container,Icon,Button,Subtitle,Header,Content,Body,Right,Title,Item,Input} from 'native-base';
import color from '../Helpers/color';
import { Actions,Router,Scene } from 'react-native-router-flux';
import ModalComp from '../components/ModalComp';
export default class SetPassword extends React.Component{
	constructor(props){
		super(props);
		this.state={pass:'',cpass:''}
	}
	loaddisclaimer=()=>{
		// Actions.signup();
		// var obj={'type':'disclaimer'};
		var obj=this.props.signupdata;
		obj.pass=this.state.pass;
		obj.type='disclaimer';
			// alert(JSON.stringify(obj));
			if(this.state.pass==""||this.state.cpass==""){
				 ToastAndroid.show('Field is Empty', ToastAndroid.SHORT);
				
			}else{
				if(this.state.pass!=this.state.cpass){
					ToastAndroid.show('Password & Confirm Password are not same ', ToastAndroid.SHORT);
				}else{
					if(this.props.loaddisclaimer){
			this.props.loaddisclaimer(obj);
		}			
				}
			}
		
	}
	passwordData=(data,key)=>{
		this.setState({[key]:data});
	}
render(){
	return(
		<Content>
		<View style={{alignItems:'center'}}>
		<View style={styles.circle}>
			<Image source={require('../images/venue.jpg')} style={[styles.innercircle]}>
			</Image>
		</View>
		<View style={styles.loginheader}>
		<Text style={styles.smalltitle}>Welcome</Text>
		<Text style={styles.logintitle}>{this.props.signupdata.name}</Text>
		<Text style={styles.loginsubtitle}>Venue Provider</Text>
		</View>
	<View style={{width:'100%',padding:12}}>
          <Item regular style={[styles.texboxpadding,styles.itemsize]}>
            <Input style={styles.textboxsize} placeholderTextColor={color.ash3}  secureTextEntry placeholder='Enter Password' onChangeText={(data)=>this.passwordData(data,'pass')} />
          </Item>
          <Item regular style={[styles.texboxpadding1,styles.itemsize]}>
            <Input style={styles.textboxsize} placeholderTextColor={color.ash3}  secureTextEntry placeholder='Re-enter Password' onChangeText={(data)=>this.passwordData(data,'cpass')}  />
          </Item>
     </View>
     <View style={styles.submitbox}>
          <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end'}}>
          <Button onPress={()=>this.loaddisclaimer()} style={[styles.actionbtn,{backgroundColor:color.orange}]}>
           <Text style={styles.actionbtntxt}>CONFIRM</Text>
          </Button>
          </View>
     </View>
     <View style={styles.sociallogin}>
	<Text style={styles.footerheading}>Password Specifications</Text>
	<Text style={styles.footersubtext}>Password Should have Alphabet, Numbers, Special Characters</Text>
	<Text style={styles.footersubtext}>Other Specification here</Text>
     </View>
		</View>
     </Content>
		)
}

}
const styles={

	circle:{
		width:hp('17%'),
		height:hp('17%'),
		borderRadius:hp('17%')/2,
		marginTop:'5%',
		borderWidth:1,
		alignItems:'center',
		justifyContent:'center',
		borderColor:color.orange,
	},
	innercircle:{
		position:'absolute',
		width:hp('15.5%'),
		height:hp('15.5%'),
		borderRadius:hp('15.5%')/2,
		margin:'auto',
		backgroundColor:color.ash,
		margin:0,
	},
	loginheader:{
		marginTop:hp('2%'),
		alignItems:'center'
	},
	smalltitle:{
fontSize:hp('2%'),
		color:color.black1
	},
	logintitle:{
		fontSize:hp('3%'),
		color:color.orange
	},
	loginsubtitle:{
		fontSize:hp('2.3%'),
		color:color.black,
	},
	texboxpadding:{
		marginBottom:24,
	},
	texboxpadding1:{
		marginBottom:10,
	},
	submitbox:{
		flex:1,
		flexDirection:'row',
		paddingLeft:12,
		paddingRight:12,
		paddingBottom:15,
		borderBottomWidth:0.5
	},
	actionbtn:{
		borderRadius:5,
		width:hp('19%'),
		backgroundColor:color.orangeBtn,
		justifyContent:'center',
	},
	actionbtntxt:{
		textAlign:'center',
		color:color.white,
		fontSize:hp('2.3%')
	},
	sociallogin:{
		backgroundColor:color.ash1,
		height:hp('24%'),
		width:'100%',
		alignItems:'flex-start',
		padding:18,
		justifyContent:'space-between'
	},
	socialiconlogin:{
		marginTop:10,
		flexDirection:'row'
	},
	socialicon:{
		width:hp('4%'),
		height:hp('4%'),
		margin:5
	},
	footerheading:{
		fontSize:hp('3%'),
		fontWeight:'bold',
	},
	footersubtext:{
		fontSize:hp('2.2%'),
		color:color.ash3
	},
	textboxsize:{
		fontSize:hp('2.2%')
	},
	itemsize:{
		height:hp('5.5%')
	}
}
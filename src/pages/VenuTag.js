'use strict';

import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import color from '../Helpers/color';
import {Left,List,Container,Icon,Button,Subtitle,Header,Content,Body,Right,Title,Item,Input, ListItem, CheckBox,Accordion } from 'native-base';
import {
  StyleSheet,
  View,
  Text,
  Picker,
  TextInput,
  TouchableOpacity,Image
} from 'react-native';
import links from '../Helpers/config';
import PhotoPlus from '../images/svg/plus.png';

 class VenueTags extends Component {

  constructor(props) {
    super(props);
   const newtags = JSON.parse(JSON.stringify(props.mytags))
    this.state = {mynwtags:newtags,amentiesvisible:false,activeId:1,selected:'key0',tags:[]};
    // alert(datas.length%2);
  }
  onValueChange=(value)=>{
    this.setState({
      selected: value
    });
  }

  componentWillMount(){
    alert('hiii');

    this.props.labeltext({labeltext:<Text><Text style={{color:color.orange}}>{this.props.roomname}</Text> Please tag related <Text style={{color:color.orange,fontWeight:'bold'}}> Keyword </Text> </Text>});
  }
  sendtags=()=>{
    const newArray = this.state.mynwtags.map(a => Object.assign({}, a));
    var filtertags=newArray.filter((obj)=>{
      return obj.tags.some( function (obj1) {
      return obj1.state==true
    });
    })
  filtertags.map((obj)=>{
  obj.tags=obj.tags.filter((obj1)=>obj1.state==true)});
  this.props.sendTags(filtertags);


  }
  changeTextBox=(i1,i2,state)=>{
    var mynwtags=this.state.mynwtags;
    mynwtags[i1].tags[i2]['state']=!state;
    this.setState({mynwtags});
    this.sendtags();

  }
  addtags=(textkey,tagkey)=>{
    if(this.state[textkey]){
      var mynwtags=this.state.mynwtags;
      mynwtags[tagkey].tags.push({id:mynwtags[tagkey].tags.length+1,name:this.state[textkey],state:true});
      this.setState({mynwtags});
      this.setState({[textkey]:null});
    this.sendtags();
    }
  }
    loadTags=()=>{
alert("apicalling");
    fetch(links.APIURL+'getTags', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body:JSON.stringify({user_cat_id:"1"}),
    }).then((response)=>response.json())
    .then((responseJson)=>{

      // var response=responseJson.data;
      // console.log("response datas",response);
      // response.map(v => v.data.map(d => d.isChecked = false));
      // console.log(response)
      alert(JSON.stringify(responseJson));
      // this.setState({ tags: response,loading:false});

    }) 
  }
  render(){
    return(
      <View style={{flex:1,marginTop:0,padding:10}}>
      {this.state.mynwtags.map((obj,key1)=>{
        return(
 <View key={key1} style={styles.containerview}>
         <View style={styles.addContainer}>
           <TextInput placeholder={obj.name} value={this.state['key'+key1]} onChangeText={(text)=>{this.setState({['key'+key1]:text})}} style={styles.addButtonColor} />
           {/*<TouchableOpacity disabled={!this.state['key'+key1]} onPress={()=>this.addtags('key'+key1,key1)}><Icon type="MaterialIcons" style={{fontSize:hp('5%'),color:this.state['key'+key1]?color.orange:color.black1}} name="add-circle-outline"/></TouchableOpacity>*/}
           <TouchableOpacity disabled={!this.state['key'+key1]} onPress={()=>this.addtags('key'+key1,key1)}><Image source={PhotoPlus} tintColor={color.orange} style={{width:hp('4.5%'),height:hp('4.5%')}}/></TouchableOpacity>
         </View>
         <View style={{flexDirection:'row',flexWrap:'wrap'}}>
         {obj.tags.map((obj,key2)=>{
           return(
            <TouchableOpacity onPress={()=>this.changeTextBox(key1,key2,obj.state)} key={key2} style={[styles.tagContainer,obj.state==true?styles.activeBG:'']}>
         <Text style={[styles.containerText,obj.state==true?styles.acitveText:'']}>{obj.name}</Text>
         </TouchableOpacity>
         )
         })}
        
         </View>
         </View>
          )

      })}
     
        
        
      </View>
      )
  }

  componentDidMount(){
    alert("hii");
    this.loadTags();
  }
}
const styles={
  tagContainer:{
    borderColor:color.orange,marginRight:12,alignSelf: 'flex-start',borderWidth:0.7,padding:7,fontSize:hp('2.5%'),width:'auto',marginBottom:12
  },
  addButtonColor:{borderWidth:1,borderColor:color.black1,width:'80%',height:'75%',marginRight:12},
  addContainer:{flexDirection:'row',alignItems:'center',width:'100%',marginBottom:5},
  containerText:{fontSize:hp('2%'),color:color.blueactive},
  activeBG:{backgroundColor:color.orange},
  acitveText:{color:color.white},
  containerview:{borderBottomWidth:0.6,borderColor:color.black1,paddingTop:5,paddingBottom:5}
}
export default VenueTags;
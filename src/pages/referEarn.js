'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text
} from 'react-native';
import color from '../Helpers/color';

import refer  from '../images/referearn.png';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


class ReferEarn extends Component {
  render() {
    return (
      <View style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
      <TouchableOpacity>
      <Image source={refer} style={{width:hp('20%'),height:hp('20%')}}/>
      <Text style={{color:color.black1,fontSize:hp('3%'),textAlign:'center'}}>Refer & Earn</Text>
      </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({

});


export default ReferEarn;
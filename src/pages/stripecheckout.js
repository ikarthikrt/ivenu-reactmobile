'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import { Actions, Router, Scene } from 'react-native-router-flux';

import StripeCheckout  from '../Helpers/react-stripe-checkoutlib';
import links from '../Helpers/config';
import Toast from 'react-native-simple-toast';

class Stripecheckout extends Component {
	constructor(props) {
	  super(props);
		let bookingData=props.navigation.state.params.bookingData;
    let bookdetails=props.navigation.state.params.bookdetails;
    let priceamount=props.navigation.state.params.priceamount;
	    this.state = {venue_name:bookingData.trn_venue_name,amt:priceamount,category:bookingData.venue_cat_name,img:bookingData.photos.length>0?bookingData.photos[0].venue_image_path:require('../images/imageloader.png'),currency:bookingData.price[0].trn_venue_price_currency,venue_id:bookingData.venue_id,bookdetails:bookdetails};
	}
	onPaymentSuccess=(data)=>{
console.log("request",data);
// Actions.pop();
var bookdetails=this.state.bookdetails;
// bookdetails.token=data;
bookdetails.currency=this.state.currency;
bookdetails.amt=this.state.amt*100;
var tokendata=JSON.parse(data);
tokendata.bookdetails=bookdetails;
            this.props.responseCallBack("Loading",2);
            // Actions.pop();
fetch(links.APIURL+"charge/", {
        headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
      method: 'POST',
      body: JSON.stringify(tokendata),
    }).then(response => {
      response.json().then(data => {
       console.log(data);
       if(data.error){
       this.props.responseCallBack("Payment Failed "+data.error,2)
       // Actions.pop()
       }else{
this.props.responseCallBack(this.state.venue_name+" Venue is booked successfully, details sent to your stripe email id,thank you.",1);
// Actions.pop()
       }
	})
}).catch((error)=>{
this.props.responseCallBack('Network Failed',2);
// Actions.pop();
       })
}

	onClose=(data)=>{
		// Actions
this.props.responseCallBack('Payment Cancelled By User',0);
// Actions.pop();
		// console.log("closed",data);
	}
  render() {
    return (
     <StripeCheckout
    publicKey="pk_live_lNbjOKY6WEvzCFIkz1kAhgSF002UZjb0bm"
    // publicKey="pk_test_TYooMQauvdEDq54NiTphI7jx"
    amount={this.state.amt*100}
    imageUrl={this.state.img}
    storeName={this.state.venue_name}
    description={this.state.category}
    currency={this.state.currency}
    allowRememberMe={true}
    onPaymentSuccess={this.onPaymentSuccess}
    onClose={this.onClose}
  >
  </StripeCheckout>
    );
  }
}

const styles = StyleSheet.create({

});


export default Stripecheckout;
'use strict';

import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import color from '../Helpers/color';
import DatePicker from 'react-native-datepicker';
import {Platform, StyleSheet, Text, View,Alert,StatusBar,Modal,Image,TouchableHighlight,TouchableOpacity} from 'react-native';
import {Container,Icon,Button,Subtitle,Header,Content,Body,Right,Title,Item,Input,Label,Textarea,Form} from 'native-base';
const jsCoreDateCreator = (dateString) => { 
  // dateString *HAS* to be in this format "YYYY-MM-DD HH:MM:SS"
  let dateParam = dateString.split(/[\s-:]/)  
  dateParam[1] = (parseInt(dateParam[1], 10) - 1).toString()  
  if(new Date(...dateParam)=='Invalid Date'){
    return null;
  }else{
    
  return new Date(...dateParam)  
  }
  // if(isNaN(date))
}

class FromToDuration extends Component {
  constructor(props) {
    super(props);
    this.state = {from:'',to:''};
  }
  componentWillReceiveProps=(props)=>{
    // alert(JSON.stringify(props));
    if(props.sendDateObj){
      this.setState({from:new Date(props.sendDateObj.from)=="Invalid Date"?jsCoreDateCreator(props.sendDateObj.from):props.sendDateObj.from});
      this.setState({to:new Date(props.sendDateObj.to)=="Invalid Date"?jsCoreDateCreator(props.sendDateObj.to):props.sendDateObj.to});

    }
  }
  setDate=(date,key)=>{
    // alert(date);
// var jsondate=JSON.stringify(new Date(date));
    this.setState({[key]:date});
    var data=this.state;
    data[key]=date;
    this.props[key](data);
  }
  render() {
    return (
    	<View style={{padding:12,marginVertical:12}}>
      <View style={{flex:1,flexDirection:'row',alignItems:"center",marginBottom:16}}>
      <View style={[styles.rowpadding,{flex:0.23}]}>
      <Text style={styles.fromtotext}>From</Text>
      </View> 
      <View style={[styles.rowpadding,{flex:0.77}]}>
        
        <View style={{width:'100%',height:'100%'}}>
       <DatePicker
        style={{width: '100%',textAlign:'left'}}
        date={this.state.from}
        mode="datetime"
        placeholder="select date"
        format="MMM DD YYYY hh:mm a"
        androidMode="spinner"
        minDate={new Date()}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        iconComponent={<Image  style={{width:hp('4%'),height:hp('4%'),marginLeft:12}} source={require('../images/svg/calendar.png')}/>}
        customStyles={{
          dateIcon: {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 0,
          },
          dateInput: {
            borderWidth:0.5,borderColor:color.black1,
            marginLeft: 0,
            paddingLeft:10,
            textAlign:'left',
            alignItems:'flex-start'
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={(date) => this.setDate(date,'from')}
      />
			</View>
      </View>
      </View>
         <View style={{flex:1,flexDirection:'row',alignItems:"center"}}>
      <View style={[styles.rowpadding,{flex:0.23}]}>
      <Text style={styles.fromtotext}>To</Text>
      </View> 
      <View style={[styles.rowpadding,{flex:0.77}]}>
     
        <View style={{width:'100%'}}>
         <DatePicker
        style={{width: '100%',textAlign:'left'}}
        date={this.state.to}
        mode="datetime"
        androidMode="spinner"
        minDate={new Date(this.state.from)}
        placeholder="select date"
        format="MMM DD YYYY hh:mm a"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        iconComponent={<Image  style={{width:hp('4%'),height:hp('4%'),marginLeft:12}} source={require('../images/svg/calendar.png')}/>}

        customStyles={{
          dateIcon: {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 0,
          },
          dateInput: {
            borderWidth:0,
             borderWidth:0.5,borderColor:color.black1,
            marginLeft: 0,
            paddingLeft:10,
            textAlign:'left',
            alignItems:'flex-start'
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={(date) => this.setDate(date,'to')}
      />
      </View>
      
      
      </View>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
rowpadding:{
	paddingLeft:hp('2%'),
	paddingRight:hp('2%')
},
fromtotext:{
	fontSize:hp('2%'),
	color:color.orange
},itemsize:{
		height:hp('5.5%'),
		borderColor:'white'
	}
});


export default FromToDuration;
'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  WebView,
  Text,
  ActivityIndicator,
  Animated,
  NetInfo
} from 'react-native';
import {
Accordion,
Container,
Content,
List,
ListItem,
Thumbnail,
Left,
Body,
Right,
Button,
Icon,
Footer,
FooterTab
} from "native-base";
import { Actions, Router, Scene } from 'react-native-router-flux';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import color from '../Helpers/color';
import Toast from 'react-native-simple-toast';
import LoadingOverlay from '.././components/LoadingOverlay';
import links from '../Helpers/config';
const jsCode = `(function() {
                    var originalPostMessage = window.postMessage;

                    var patchedPostMessage = function(message, targetOrigin, transfer) {
                      originalPostMessage(message, targetOrigin, transfer);
                    };

                    patchedPostMessage.toString = function() {
                      return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage');
                    };

                    window.postMessage = patchedPostMessage;
                  })();`;
class CheckoutPageWeb extends Component {
  constructor(props) {
    super(props);
  
    this.state = {loaded:null,isConnected:null,fadeIn:new Animated.Value(0),fadeOut:new Animated.Value(1)};
  }
     fadeIn() {
     Animated.timing(
       this.state.fadeIn,           
       {
         toValue: 1,                   
         duration: 1000,              
       }
     ).start(() => this.fadeOut());                        
  }

  fadeOut() {

    Animated.timing(                  
       this.state.fadeIn,            
       {
         toValue: 0,                   
         duration: 1000,              
       }
    ).start(()=>this.fadeIn());                        
  }
receiveMessageData=(data,venue_name)=>{
  var parseData=JSON.parse(data);
  // alert(parseData.status);
  if(parseData.status==true){
    Toast.show(venue_name+" Venue is booked successfully, details sent to your stripe email id,thank you.",Toast.LONG);
    setTimeout(()=>{
      Actions.reset('home');
    },200);
  }
}
 ActivityIndicatorLoadingView() {
    //making a view to show to while loading the webpage
    return (
      <View style={{flex:1,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
      <View>
      <ActivityIndicator
        color={color.top_red}
        size="large"
        style={styles.ActivityIndicatorStyle}
      />
      <Text style={{fontSize:hp('2.5%')}}>Loading Please Wait...</Text>
      </View>
      </View>
    );
  }
   handleConnectivityChange=(isConnected )=>{
    // alert(isConnected);
    this.setState({isConnected:isConnected});
  }
  render() {
    const userId=this.props.navigation.state.params.userId;
    const venueId=this.props.navigation.state.params.venueId;
    const venuename=this.props.navigation.state.params.venuename;
    const paxseatid=this.props.navigation.state.params.paxseatid;
    var appurl=links.WEBURL;
    if(paxseatid){
    var uri=appurl+'/?/checkout/id='+venueId+'&'+paxseatid+'&mobilefor_payment&userId='+userId
    }else{
    var uri=appurl+'/?/checkout/id='+venueId+'&mobilefor_payment&userId='+userId
    }
    return (
      <View style={{flex:1,backgroundColor:'grey'}}>
       {this.state.isConnected==true&&
       <WebView
       javaScriptEnabled={true}
        scrollEnabled={false}
        bounces={false}
        userAgent="Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36"
      onLoadEnd={()=>this.setState({loaded:true})}
        //Want to show the view or not
        startInLoadingState={true}
       renderLoading={this.ActivityIndicatorLoadingView}
      onMessage={(event)=> this.receiveMessageData(event.nativeEvent.data,venuename)}
        source={{uri:uri }}
      >
      </WebView>
    }
     {this.state.isConnected==false&&
      <View style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'center',backgroundColor:'white'}}>
        <Animated.View style={{opacity:this.state.fadeIn,flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
        <Icon type="FontAwesome" name="wifi" style={{color:color.top_red,fontSize:hp('5%')}}/>
        <Text  style={{color:color.top_red,fontSize:hp('2.5%')}}>No Internet Available</Text>
        </Animated.View>
      </View>
    }
      </View>
    );
  }
  componentDidMount(){
  // alert('');
  this.fadeIn();
  var self=this;
  NetInfo.fetch().then(state => {
    // alert(JSON.stringify(state));
    if(state=="NONE"){
    self.setState({isConnected:false})
    }else{
    self.setState({isConnected:true})
    }
});
  NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
}
componentWillUnmount() {
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
 }
}

const styles = StyleSheet.create({

});


export default CheckoutPageWeb;
'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Container, Header, Left, Body, Right, Button, Icon, Title,Content,ActionSheet,Root,Toast,Footer ,FooterTab} from 'native-base';

import color from '../Helpers/color'


class CorporateReview extends Component {
	returnData=(data)=>{
		var stringData="";
		if(data){
			for(var i in data){
				if(data[i].spocData){
					stringData+=data[i].spocData.venue_spec_name+" in "+data[i].name+",";
				}
			}
		}
		return stringData;
	}
	componentWillMount() {
		// alert(JSON.stringify(this.props.loginDetails))
        this.props.labeltext({ labeltext: <Text>Before Submitting <Text style={{color:color.orange,fontWeight:'bold'}}>Review the  Details </Text></Text> });
    }
	submitCoproateVenue=()=>{
		// alert(JSON.stringify)
		var spoccheck=this.props.locationArray?this.props.locationArray.filter((obj)=>obj.added==true):[];
		var locationData=this.props.locationArray?this.props.locationArray:[];
		if(this.props.facilityerror){
			if(this.props.facilityerror=='failure'){
				alert("Please Fill Up Facility Details");
				return;
			}
		}else{

				alert("Please Fill Up Facility Details");
				return;
		}
		if(locationData.length>0){
			if(spoccheck.length==locationData.length){

			}else{
			alert("SPOC Details Not Completely Added");
			return
			}
		}else{
			alert("SPOC Details Not Completely Added");
			return;
		}
		var location_array=[];
		var corpobj={"user_id":this.props.loginDetails.user_id,"venue_cat_id":this.props.venuedetails.venue_cat_id,"venue_name":this.props.commonArray[1].spec_det_datavalue1,location_details:[]}
		spoccheck.map((obj)=>corpobj.location_details.push({venue_spec_id:obj.spocData.venue_spec_id, "venue_location":"1.8565 , 2.7854","location_id":obj.id,spoc_name:obj.spocData.spocData[0].spec_det_datavalue1,spoc_email:obj.spocData.spocData[1].spec_det_datavalue1,spoc_mobile:obj.spocData.spocData[2].spec_det_datavalue1}));
		// alert(JSON.stringify(corpobj));
		this.props.submitCorporat(corpobj);

	}
	editclick=()=>{
		this.props.editclick();
	}
  render() {
    return (
      <View style={{padding:20}}>
      <View style={[styles.row,{flex:1,flexDirection:'row'}]}>
      <View style={{flex:0.4}}><Text style={styles.fonthead}>Corporate Name</Text></View>
      <View style={{flex:0.6}}><Text style={styles.font_headvalue}>{this.props.venuedetails&&this.props.venuedetails.venue_cat_name}</Text></View>
      </View>
      <View style={[styles.row,{flex:1,flexDirection:'row'}]}>
      <View style={{flex:0.4}}><Text style={styles.fonthead}>Locations</Text></View>
      <View style={{flex:0.6}}><Text style={styles.font_headvalue}>{this.props.locationArray&&this.props.locationArray.map((obj)=>obj.name).join(',')}</Text></View>
      </View>
      <View style={[styles.row,{flex:1,flexDirection:'row'}]}>
      <View style={{flex:0.4}}><Text style={styles.fonthead}>Facilities</Text></View>
      <View style={{flex:0.6}}><Text style={styles.font_headvalue}>{this.returnData(this.props.locationArray)}</Text></View>
      </View>
      <View style={[styles.row,{flex:1,flexDirection:'row'}]}>
      <View style={{flex:0.4}}><Text style={styles.fonthead}>Total Autorization</Text></View>
      <View style={{flex:0.6}}><Text style={styles.font_headvalue}>{this.props.locationArray&&this.props.locationArray.length}</Text></View>
      </View>
            <View style={styles.submitbox}>
     <View style={{flex:0.5,flexDirection:'row',justifyContent:'flex-start'}}>
       <Button  onPress={this.editclick} style={[styles.actionbtn,{backgroundColor:color.orange}]}>
            <Text style={styles.actionbtntxt}>EDIT</Text>
          </Button>
          </View>
          <View style={{flex:0.5,flexDirection:'row',justifyContent:'flex-end'}}>
          <Button onPress={()=>this.submitCoproateVenue()} style={[styles.actionbtn,{backgroundColor:color.orange}]}>
           <Text style={styles.actionbtntxt}>SUBMIT</Text>
          </Button>
          </View>
     </View>
      </View>
    );
  }
}

const styles = {
	row:{
	marginBottom:20
	},
  fonthead:{
    fontSize:hp('2.2%')
  },
  font_headvalue:{
    fontSize:hp('2.2%'),
    fontWeight:'bold'

  },
   submitbox:{
    flex:1,
    flexDirection:'row',
  },
    actionbtn:{
    borderRadius:5,
    width:hp('12%'),
    justifyContent:'center',
  },
  actionbtntxt:{
    textAlign:'center',
    color:color.white,
    fontSize:hp('2.3%')
  },
};


export default CorporateReview;
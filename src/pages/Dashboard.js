'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
     RefreshControl,
  ScrollView
} from 'react-native';
import { Actions, Router, Scene } from 'react-native-router-flux';
import DateFunctions from '../Helpers/DateFunctions';
import color from '../Helpers/color';
import { Container, Header, Content, Tab, Tabs } from 'native-base';
import dashboardimage from '../images/bookings.png';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import BookingCard from '../components/BookingCard'
import links from '../Helpers/config';
import AsyncStorage from '@react-native-community/async-storage';
import moment from "moment";
class Dashboard extends Component {
  constructor(props) {
    super(props);
  
    this.state = {bookingDetails:null,refreshing:false};
  }
   bookdetails=(data,status)=>{
     // alert(JSON.stringify(data));
     Actions.push('bookingmoredetails',{bookdetails:data,status:status});
   }
  render() {
    const {bookingDetails} =this.state;

    return (
       <Container>
        <Tabs >
          <Tab heading="Confirmed">
          <ScrollView style={{flex:1}} refreshControl={
          <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />
        }>
          {!this.state.refreshing&&(!bookingDetails ||bookingDetails&&bookingDetails.length==0)&&
            <View style={{flex:1,marginTop:12}}><Text style={{textAlign:'center',fontSize:hp('2.6%'),color:color.top_red}}>No Bookings Were Found</Text></View>
          }
          {bookingDetails&&bookingDetails.length>0&&bookingDetails.filter((obj1)=>obj1.trn_booking_status==1).map((obj)=>{
            var dateonbooked=(obj.trn_booked_date)?moment(DateFunctions.getMobileDate(obj.trn_booked_date)).format('DD MMM / YYYY'):"-";
            var venuetype=obj.trn_venue_type==2?'Pax':(obj.trn_venue_type==3?'Seat':'Venue');
            return(
             <BookingCard onClick={()=>this.bookdetails(obj,'CONFIRMED')} subtitle={venuetype} leftHeader={obj.trn_venue_name.toUpperCase()} rightHeader={dateonbooked}>
             <View style={styles.bodycard}>
             <View>
             {(obj.trn_venue_type==2||obj.trn_venue_type==3)&&
             <Text style={{color:color.blueactive,fontWeight:'bold',fontSize:hp('2%')}} >{(obj.trn_venue_type==2?obj.pax_name +` - (${obj.pax_qty} Paxes)`:obj.seat_name+` - (${obj.seat_qty} Seats)`).toUpperCase()} </Text>
             }
             <Text style={{color:color.blueactive,fontSize:hp('2%')}} >BOOKING ID : {obj.booking_uniqId}</Text>
             </View>
             <Text style={{color:color.top_red,fontSize:hp('2%')}}>CONFIRMED</Text>
             </View>
             </BookingCard>
           )
         })
         }
            </ScrollView>
          </Tab>
          <Tab heading="Cancelled">
          <ScrollView style={{flex:1}}  refreshControl={
          <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />
        }>
            {!this.state.refreshing&&(!bookingDetails ||bookingDetails&&bookingDetails.length==0)&&
            <View style={{flex:1,marginTop:12}}><Text style={{textAlign:'center',fontSize:hp('2.6%'),color:color.top_red}}>No Bookings Were Found</Text></View>
          }
            {bookingDetails&&bookingDetails.length>0&&bookingDetails.filter((obj1)=>obj1.trn_booking_status==2).map((obj)=>{
            var dateonbooked=(obj.trn_booked_date)?moment(DateFunctions.getMobileDate(obj.trn_booked_date)).format('DD MMM / YYYY'):"-";
            var venuetype=obj.trn_venue_type==2?'Pax':(obj.trn_venue_type==3?'Seat':'Venue');
            return(
             <BookingCard onClick={()=>this.bookdetails(obj,'CANCELLED')} subtitle={venuetype} leftHeader={obj.trn_venue_name.toUpperCase()} rightHeader={dateonbooked}>
             <View style={styles.bodycard}>
             <View>
             {(obj.trn_venue_type==2||obj.trn_venue_type==3)&&
             <Text style={{color:color.blueactive,fontWeight:'bold',fontSize:hp('2%')}} >{(obj.trn_venue_type==2?obj.pax_name +` - (${obj.pax_qty} Paxes)`:obj.seat_name+` - (${obj.seat_qty} Seats)`).toUpperCase()} </Text>
             }
             <Text style={{color:color.blueactive,fontSize:hp('2%')}} >BOOKING ID : {obj.booking_uniqId}</Text>
             </View>
             <Text style={{color:color.top_red,fontSize:hp('2%')}}>CANCELLED</Text>
             </View>
             </BookingCard>
           )
         })
         }
          </ScrollView>

          </Tab>
         
        </Tabs>
      </Container>
    );
  }
  bookingdetails=(userid)=>{
    // alert(links.APIURL+"myBookingDetails");
    fetch(links.APIURL+"myBookingDetails",{
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userId:userid

      }),
    })
    .then((response)=>response.json())
    .then((responsejson)=>{
      // console.log(responsejson);
      this.setState({refreshing:false});
      this.setState({bookingDetails:responsejson.data})
      // alert(JSON.stringify(responsejson));
    })
  }
   onRefresh=async()=>{
    this.setState({refreshing:true},async function(){
       var checklogindetails = await AsyncStorage.getItem(
                     "loginDetails"
                   );
      if(checklogindetails!=null){
          const loginDetails = JSON.parse(
                       await AsyncStorage.getItem("loginDetails") 

                     );
          // alert(JSON.stringify(loginDetails));
          this.bookingdetails(loginDetails.user_id);
        }
    })

  }
  componentDidMount =async()=>{
   var checklogindetails = await AsyncStorage.getItem(
                     "loginDetails"
                   );
      if(checklogindetails!=null){
          const loginDetails = JSON.parse(
                       await AsyncStorage.getItem("loginDetails") 

                     );
          // alert(JSON.stringify(loginDetails));
          this.bookingdetails(loginDetails.user_id);
        }
    
  }
}

const styles = StyleSheet.create({
bodycard:{
  flex:1,
  flexDirection:'row',
  justifyContent:'space-between',
  alignItems:'center'
}
});


export default Dashboard;
import React,{Component} from 'react';
import {View,Text,StyleSheet,Picker,TouchableOpacity,Image,Alert,FlatList,ScrollView} from 'react-native';
import {} from 'native-base'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import color from '../Helpers/color';
import Down_arrow from '../images/arrow_down.png'
import StarRating  from 'react-native-star-rating'
import Tick_mark from '../components/TickComp'
import ListingDetails from './ListingDetails';
import Triangle from '../components/triangles';
import Line from '../components/Line';
import { Actions,Router} from 'react-native-router-flux'
import VenueCardComp from '../components/VenueCardComp';

import placeholder_img from '../images/placeholder_img.png';
const near_ground_list=[{id:1,starCount:3,image:require('../images/slider1.png'),ground_name:'Brand new indoor soccer center',kms:'10 kms',mins:'35 mins'},
                        {id:2,starCount:4,image:require('../images/slider2.png'),ground_name:'Soccer Club Academy',kms:'5 kms',mins:'15 mins'},
                        {id:3,starCount:2.5,image:require('../images/slider3.png'),ground_name:'Best Soccer Pitche Ground',kms:'5 kms',mins:'15 mins'},
                        {id:4,starCount:4,image:require('../images/slider4.png'),ground_name:'Brand new indoor soccer center',kms:'6 kms',mins:'20 mins'}
                    ] 
  isCloseToRight = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToRight = 10;
    // alert(layoutMeasurement.width + contentOffset.x)
    return layoutMeasurement.width + contentOffset.x >= contentSize.width - paddingToRight;
};
export default class GroundNearYou extends Component{
    constructor(props) {
        super(props);
        this.state= {
            slectdata:'',
            visible: false,
            sliderData:[],listingdetails:false,activeCategory:null,showvisible:false
        }
        
    }
    onPress=(item,index)=>{
    // alert(JSON.stringify(item))
    // alert(this.props.latlng);
    Actions.BookDetails({
                             lat: this.props.latlng.latitude,
                             long: this.props.latlng.longitude,
                             id: item.venue_id
                           }) 
     // var sliderData=this.state.sliderData;
     // sliderData.map((data)=>data.visible=false);
     // this.setState({listingdetails:false});
     // this.setState({activeCategory:item});
     // sliderData[index].visible=true;
     // // alert(JSON.stringify(item));
     // this.setState({sliderData});
     //    //     Alert.alert(item.ground_name)
     //  //  this.setState({isHidden: !this.state.isHidden})
     //    this.setState({visible:true})
     //    this.setState({showvisible:true});
     //    this.setState({listingdetails:true});
     //      this.setState({slectdata:item.id});
      }
   
componentWillMount=()=>{
    if(this.props.sendsliderdata){

this.setState({sliderData:this.props.sendsliderdata})
    }
}
      onStarRatingPress(rating) {
        this.setState({
          starCount: rating
        });
      }
       componentWillReceiveProps(props){
         if(props.sendsliderdata){
         this.setState({sliderData:props.sendsliderdata})
         
            
         
     }
     if(!props.arrowclicked){
     }else{

        this.setState({activeCategory:null})
     }
     // alert(JSON.stringify(props.sendsliderdata))
      }

    render(){
       
        return(
            <View style={styles.container}>
           
                <View style={styles.flex_row}>
                    <Text style={styles.text_style1}>{this.state.sliderData&&this.state.sliderData.length>0&&this.state.sliderData[0].venue_cat_name}</Text>
                    <Text style={styles.text_style}>Near You </Text>
                    <View style={{flexDirection:'column',justifyContent:'center',marginLeft:10}}>
                        {/*<Image source={Down_arrow} style={styles.Down_arrowstyle}></Image>*/}
                    </View>
                </View>
                <ScrollView ref={(c) => {this.scrollimage = c}}  onScroll={({nativeEvent}) => {
      if (isCloseToRight(nativeEvent)) {
        // enableSomeButton();
        this.props.sendScrollEnd&&this.props.sendScrollEnd(true);
        var self=this;
        // setTimeout((obj)=>{
        // self.scrollimage.scrollToEnd();
        // },200)
        // alert("reached end")
      }
    }} directionalLockEnabled={false} horizontal={true} pagingEnabled={false}  showsHorizontalScrollIndicator={false} >
               
                        {this.state.sliderData&&this.state.sliderData.map((item,index)=>{
                            return(
                                <View style={styles.square}>
                        <TouchableOpacity       
                            onPress={()=>this.onPress(item,index)}                 
                              > 

                           
                    
                          <VenueCardComp item={item} star={true}/>  
                           
                        </TouchableOpacity>
                       
                    </View>
                                )
                        })}
                    {this.props.children}
                    </ScrollView>
                    <Line />
                    
                   {this.state.activeCategory&&
                    <ListingDetails activeCategory={this.state.activeCategory} />
                }
                

            </View>
        )
    }
}
const styles=StyleSheet.create({
    container:{
        backgroundColor:color.white
    },
    flex_row:{
        flexDirection:'row'
    },
    text_style1:{
        fontSize:hp('2%'),
        marginRight:10
    },
    text_style:{
        color:color.blue,
        fontSize:hp('2%')
    },
    Down_arrowstyle:{
        height:hp('1.4%'),
        width:hp('1.4%')
    },
    RatingBar:{
       height:hp('2.5%'),
       width:wp('13%')
    },
    _starstyle:{
      fontSize:hp('1.8%'),
        marginRight:1,
      },
    square:{
        margin:5,
        width:hp('15%'),
        
    },
    image_style:{     
        height:hp('10%'),
        width:hp('15%'),
        borderRadius:5,
        justifyContent:'center',
            
    },
    text_style2:{
      //  textAlign:"center",
        fontSize:hp('1.6%'),
    },
    text_style3:{
        color:color.orange,
        fontSize:hp('1.4%')
    },
    selected:{
        backgroundColor:color.blue
    },
    activeparent: { position: 'absolute', bottom:0, alignItems: 'center', justifyContent: 'center', zIndex: 999999,margin:'auto',left:0,right:0 }

  
})
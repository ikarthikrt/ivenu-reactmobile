'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity
} from 'react-native';
import { Actions} from 'react-native-router-flux'
import { Container, Header, Title,Subtitle, Content, Button, Icon, Left,Right,  Body,List,ListItem } from 'native-base';
import color from '../Helpers/color'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';

var itemsidebar = [
  { name: "Home", active: false, key: 0 },
  { name: "Action Items", active: false, key: 1 },
  { name: "Dashboard", active: false, key: 2 },
  { name: "Enquiries", active: true, key: 3 },
  { name: "Loyalty Program", active: false, key: 4 },
  { name: "My Calendar", active: false, key: 4 },
  { name: "Logout", active: false, key: 5 }
];
class SideBar extends Component {
	constructor(props) { 
	  super(props);
	  this.state = {sidebar:JSON.parse(JSON.stringify(itemsidebar)),loginDetails:null};
	}
  changeItem=(obj)=>{
    
    var sidebar=this.state.sidebar;
    sidebar.map((obj)=>obj.active=false);
    var index=this.state.sidebar.findIndex((data)=>data.name==obj.name);
    // alert(index);
    sidebar[index].active=true;
    this.setState({sidebar});
    if(obj.name=='Logout'){
       AsyncStorage.removeItem('loginDetails');
       Actions.drawerClose();
       Actions.home();
    }else if(obj.name=='Home'){
      Actions.home();
      Actions.drawerClose();
    }
  }
  componentDidMount(){
     AsyncStorage.getItem('loginDetails', (err, result) => {
      
       if(result!=null){
    this.setState({loginDetails:JSON.parse(result)})
      }else{
    this.setState({loginDetails:null})

      }
     });
  }
  render() {
    return (
       <Container>
    <Header style={{backgroundColor:color.white,height:hp('10%')}}>
        <View style={{alignItems:'center',flexDirection:'row',marginRight:0}}>
        <Image style={styles.headerImage} source={require('../images/venue.jpg')}/>
        </View>
       <Body style={{paddingLeft:wp('3.2%')}}>
        
        <Text numberOfLines={1} style={{fontSize:hp('3.2%'),color:color.black1,width:'95%'}}>Welcome
         <Text style={{color:color.orange}}> Bharathi</Text></Text>
       
        <Text style={{color:color.black1,fontSize:hp('1.7%')}}>Venue Provider</Text>
        </Body>
        <View style={{flexDirection:'row',alignItems:'center'}}>
        <TouchableOpacity onPress={()=>Actions.drawerClose()}>
        <Icon style={{color:color.orange}} name="menu"/>
        </TouchableOpacity>
        </View>
        </Header>
         <Content>
          <List>
          {this.state.sidebar.map((obj,key)=>{
          	return(
          		 <ListItem  key={key} style={{borderWidth:0,marginLeft:0,paddingLeft:20}} onPress={()=>this.changeItem(obj)}>
              <Left>
                <Text style={{color:obj.active==true?'orange':null}}>{obj.name}</Text>
              </Left>
              <Right>

              </Right>
            </ListItem>
          		)
          })}
           
            </List>
         </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
headerImage:{
	width:hp('6%'),
	height:hp('6%'),
	borderRadius:hp('6%')/2
}
});


export default SideBar;
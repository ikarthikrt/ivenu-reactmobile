import React, {Component} from 'react';
import Home from '../pages/Home'
import { BackHandler } from "react-native";
import HeaderComp from '../components/HeaderComp'
import {Actions,Router,Scene} from 'react-native-router-flux'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Search_home from '../pages/Search_Home'
import VideoComp from '../components/VideoComp'
import Circlecomp from '../components/circlecomp'
import Venue_search from '../pages/venue_search'
import SearchListingSlider from '../components/SearchListingSlider'
import NearbyPlayGrounds from '../components/NearbyPlayGrounds'
import YourFavorites from '../components/Yourfavorites'
import ListingDetails from '../pages/ListingDetails'
import Book_your_venue from '../components/book your venue'
import Search_your_venue_form from '../components/Search your venue form'
import Stripecheckout from '../pages/stripecheckout'
import Search_Sccess from '../components/Search_Success Notice'
import Example from '../components/sample'
import Venue_Page  from '../pages/Venue_Page'
import VenueCategory from '../pages/Venue_Category'
import Specific_Venue from '../pages/Specific_Venue'
import SliderComp from '../components/SliderComp'
import CircleImageComp from '../components/CircleImageComp'
import Venue_Details from '../pages/Venue_Details' 
import VenueForm from '../pages/venueform';
import Dashboard from '../pages/Dashboard';
import ReferEarn from '../pages/referEarn';
import CorporateForm from '../pages/CorporateForm';
import CorporateVenue from '../pages/CorporateVenue'
import MyVenues from '../pages/MyVenues'
import SideBar from '../components/SideBar'  
import PromoCode from '../pages/promocode'; 
import BookDetails from '../pages/BookDetails';
import CheckoutPageWeb from '../pages/checkoutPageWeb';
import MyCalendar from '../pages/myCalendar';
import MyMessage from '../pages/myMessage';
import SingleChat from '../pages/singleChat';
import MyBookings from '../pages/mybookings';
import BookingMoreDetails from '../pages/bookingmoredetails';


{
  /*
   Sathish Compoenent
  */
}
import Toast from "react-native-simple-toast";
import SlotsBooking  from '../pages/slotsBooking'
import WeekAvailability from "../pages/weekAvailability";
import MonthAvailability  from '../pages/monthAvailability';
import MyCalendarWeb  from '../pages/mycalendarweb';
import Receipt from '../pages/receipt';
import AddForm from "../pages/addForm";
 let backButtonPressedOnceToExit = false;
const Routes = () => (
  <Router navBar={HeaderComp}
    backAndroidHandler={() => {
      console.log(backButtonPressedOnceToExit);
      if (backButtonPressedOnceToExit) {
        BackHandler.exitApp();
      } else {
        if (Actions.currentScene !== "home") {
          Actions.pop();
          return true;
        } else {
          backButtonPressedOnceToExit = true;
          Toast.show("Press Back Button again to exit", Toast.LENGTH_LONG);
          setTimeout(() => {
            backButtonPressedOnceToExit = false;
          }, 2000);
          return true;
        }
      }
    }}
    >
    <Scene
      key="drawer"
      drawer={true}
      initial={true}
      contentComponent={SideBar}
      drawerPosition={"right"}
      drawerWidth={wp("85%")}
    >
      <Scene key="root">
        <Scene
          key="SlotsBooking"
          component={SlotsBooking}
          title="SlotsBooking"
        />

        <Scene
          key="WeekAvailability"
          component={WeekAvailability}
          title="WeekAvailability"
        />

        <Scene
          key="MonthAvailability"
          title="MonthAvailability"
          component={MonthAvailability}
        />

        <Scene
          component={Receipt}
          title="Receipt"
          key="Receipt"
          initial={true}
        />

        <Scene component={AddForm} title="AddForm" key="AddForm" />

        <Scene component={BookDetails} title="BookDetails" key="BookDetails" />

        <Scene component={PromoCode} title="PromoCode" key="PromoCode" />

        <Scene key="home" component={Home} title="Home" initial={true}  />
        <Scene key="venueform" component={VenueForm} />
        <Scene key="corporateform" component={CorporateForm} />
        <Scene key="Search_Home" component={Search_home} />
        <Scene key="Venue_search" component={Venue_search} />
        <Scene key="venuepage" component={Venue_Page} />
        <Scene key="GroundNearYou" component={CorporateVenue} />
        <Scene key="listingdetails" component={ListingDetails} />
        <Scene key="dashboard" component={Dashboard} />
        <Scene key="referearn" component={ReferEarn} />
        <Scene key="myvenues" component={MyVenues} />
        <Scene key="messageData" component={MyMessage} />
        <Scene key="SingleChat" component={SingleChat} />
        <Scene key="MyCalendar" component={MyCalendar}  title="MyCalendar"  />
        <Scene key="checkoutpage" component={CheckoutPageWeb} />
        <Scene key="bookingmoredetails" component={BookingMoreDetails}/>
        <Scene key="mycalendarweb" component={MyCalendarWeb}/>
        <Scene
          hideNavBar={true}
          key="stripecheckout"
          component={Stripecheckout}
        />
      </Scene>
    </Scene>
  </Router>
);
export default Routes;
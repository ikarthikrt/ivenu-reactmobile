/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,TouchableOpacity,PermissionsAndroid} from 'react-native';
import Routes from './src/routers/routes.js'
import VenueForm from './src/pages/venueform';
import Geolocation from "react-native-geolocation-service";
import SplashScreen from 'react-native-splash-screen'
import AsyncStorage from "@react-native-community/async-storage";
import {
  setCustomView,
  setCustomTextInput,
  setCustomText,
  setCustomImage,
  setCustomTouchableOpacity
} from 'react-native-global-props';
import Nolocation from './src/pages/nolocation';
import WaitingLocation from './src/pages/waitinglocation';
const customTextProps = {
  style: {
    fontFamily: 'SF-Pro-Display-Regular'
  }
};
setCustomText(customTextProps);
setCustomTextInput(customTextProps);

export default class App extends Component {
  constructor(props) {
    super(props);
  
    this.state = {query:'',locationcheck:null};
  }
  requestPayment = () => {
    // return stripe
    //   .paymentRequestWithCardForm()
    //   .then(stripeTokenInfo => {
    //     console.warn('Token created', { stripeTokenInfo });
    //   })
    //   .catch(error => {
    //     console.warn('Payment failed', { error });
    //   });
  };
   requestAccess=async ()=>{
      const granted= await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,{})
      
      if(granted===PermissionsAndroid.RESULTS.GRANTED){
        // alert(true);
        this.setState({locationcheck:null})
        var self=this;
         Geolocation.getCurrentPosition(
           async position => {
             await AsyncStorage.getItem('chosenlocation', async (err, coordinates) => {
         if (coordinates == null || coordinates == "" || coordinates == undefined) {
           console.log("didmountcallapi");
           await AsyncStorage.setItem(
               "chosenlocation",
               JSON.stringify({
                 lat: position.coords.latitude,
                 lng: position.coords.longitude
               })
             );
             await AsyncStorage.setItem(
               "manualaddress",
               JSON.stringify({
                 latitude: position.coords.latitude,
                 longitude: position.coords.longitude
               })
             );
         }else{
             
         }
            self.setState({locationcheck:true})
         

       }) 
            //  alert(JSON.stringify(position));
            //  await AsyncStorage.setItem(
            //    "chosenlocation",
            //    JSON.stringify({
            //      lat: position.coords.latitude,
            //      lng: position.coords.longitude
            //    })
            //  );
            // self.setState({locationcheck:true})

           },

           error => {
             // See error code charts below.
             console.log(error.code, error.message);
             self.requestAccess();
           },
           { timeout: 10000, maximumAge: 0 }
         );

        // setTimeout(()=>{

        // // self.setState({locationcheck:true})
        // },0);
       // return true;
      }else{
        this.setState({locationcheck:'noloc'})
        // return false;
      }
  }
  componentWillMount(){
    SplashScreen.hide();
    var self=this;
    setTimeout(()=>{
this.requestAccess();
    },2000)
  }
  render() {
    var data=[1,2,3,4,5,6,7,7];
    return (
      <View style={styles.container}>
{this.state.locationcheck=='noloc'&&
<Nolocation requestAccess={()=>this.requestAccess()}/>
}
      {this.state.locationcheck&&this.state.locationcheck!='noloc'&&
      <Routes/>
    }
    {!this.state.locationcheck&&
<WaitingLocation/>
}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
   
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
